import { NiveauLog } from "src/app/tools/niveau-log.enum";

export const environment = {
  production: true,
  logLevel:NiveauLog.Info,
  apiUrl:'/ranking-ng/',
  webroot:'/ranking-ng',
  pageJeux: true,
};
