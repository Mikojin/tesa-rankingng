import { NiveauLog } from "src/app/tools/niveau-log.enum";

export const environment = {
  production: true,
  logLevel:NiveauLog.Debug,
  apiUrl:'/beta/',
  webroot:'/beta',
  pageJeux: true,
};
