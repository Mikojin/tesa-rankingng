import { NiveauLog } from "src/app/tools/niveau-log.enum";

export const environment = {
  production: true,
  logLevel:NiveauLog.Info,
  apiUrl:'/tesa-ranking-ng-prod/',
  webroot:'/tesa-ranking-ng-prod',
  pageJeux: true,
};
