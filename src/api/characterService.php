<?php

/***********************************************************************
 * Service backend PHP POST.
 * Input et Output en JSON
 */

require_once "./lib/init_service.php";


/***********************************************************************
 */
function getCharacterList($input) {
  $id_game          = $input->id_game;

  $dao = new Dao();

  LibTools::log("getCharacterList idGame=".$id_game);
	$charList		= $dao->characterDao->getList($id_game);
  return $charList;
}

call();

exit;
?>
