<?php
require_once "./lib/init_service.php";


/***********************************************************************
 * prepare la liste de Player pour l'affichage
 * */
function getRanking($input) {

  $id_game          = $input->id_game;
  $date_start       = $input->date_start;
  $date_end         = $input->date_end;

  $dao = new Dao();

  // recuperation du ranking pour la saison
	$rankingList = $dao->rankingDao->getGlobalRanking($id_game, $date_start, $date_end);

  computeRank($rankingList, true);

  return $rankingList;
}

/**
 * Renvoie le ranking pour un tournois donnée
 */
function getTournamentRanking($input) {
  $id_tournament = $input->id_tournament;

  $dao = new Dao();

  // recuperation du ranking pour la saison
	$tournamentRankingList = $dao->rankingDao->getTournamentRanking($id_tournament);

  computeRank($tournamentRankingList, false);

  return $tournamentRankingList;
}

/**
 * calcule le rang du classement donné.
 * $progress : true s'il faut estimer la progression.
 */
function computeRank($rankingList, $progress=false) {
  $length = count($rankingList);
	$last_score = -1;
	$score = 0;
	$current_rank = 0;

  for($i = 0; $i < $length; $i++) {
		$player = $rankingList[$i];
    // si les points sont null alors on met 0
    if($player->points == null) {
      $player->points = 0;
    }
		$score = $player->points;


		// si le score est identique au precedent score
		if( $last_score == $score ){
			// alors on affiche un "-"
			//   et on garde le même rank que le précédent
			$player->rank_display = '-';
			$player->rank = $current_rank;
		} else {
			// sinon on update tout avec la ligne en cours
			$r = $i+1;
			$player->rank_display = $r;
			$player->rank = $r;
			$current_rank = $r;
			$last_score = $score;
		}
    $player->rank_classe="";
    if($progress) {
      // affichage de l'icone de progression du classement
      if($player->rank > $player->previous_rank) {
        $player->rank_classe="rankdown";
      } elseif($player->rank < $player->previous_rank) {
        $player->rank_classe="rankup";
      } else {
        $player->rank_classe="ranksame";
      }
    }

    // affectation de la classe css du perso joué
    // par défaut : inconnu
    if(!isset($player->cd_char)) {
      $player->cd_char = null;
      $player->character = null;
    }

	}
}

call();


exit;
?>
