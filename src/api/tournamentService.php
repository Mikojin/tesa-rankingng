<?php

/***********************************************************************
 * Service backend PHP POST.
 * Input et Output en JSON
 */

require_once "./lib/init_service.php";


/***********************************************************************
 *
 * */
function getTournamentList($input) {
  $dao = new Dao();

  $id_game = 1;
  $date_start = '2000-01-01';
  $date_end = '2100-01-01';
  if(isset($input) && isset($input->id_game)) {
    $id_game          = $input->id_game;
    $date_start       = $input->date_start;
    $date_end         = $input->date_end;
  }

  // recuperation de la liste de tournois
	$output = $dao->tournamentDao->getList($id_game, $date_start, $date_end);

	return $output;
}

function getTournament($input) {
  $dao = new Dao();

  $id_tournament = 1;
  if(isset($input) && isset($input->id_tournament)) {
    $id_tournament          = $input->id_tournament;
  }

  // recuperation du tournois
	$output = $dao->tournamentDao->get($id_tournament);

	return $output;

}

function defaultService() {
  return getTournamentList(null);
}


call();

exit;
?>
