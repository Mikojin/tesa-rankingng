<?php
?>
<?php
/*****************************************************************************
 * game.php
 * Represente un jeu
 *****************************************************************************/

class Game {
	public $id;
	public $cd_game;
	public $name;
	public $characterMap;
	public $cssFile;

	function __construct($id=null) {
		$this->id 	= $id;
	}
}


function mapperGame($row) {
	$o = new Game();
	$o->id				        = $row['id'];
	$o->cd_game		        = $row['cd_game'];
	$o->name 		      	  = $row['name'];

	return $o;
}

?>
