<?php
?>
<?php
/*****************************************************************************
 * character.php
 * Represente un utilisateur
 *****************************************************************************/

class Character {
	public $id;

	public $id_game;
	public $game;

	public $name;
	public $cd_char;

	function __construct($id=null) {
		$this->id 	= $id;
	}

	function __toString() {
		return "id=$this->id ; id_game=$this->id_game ; name=$this->name ; css=$this->cd_char";
	}
}


function mapperCharacter($row) {
	$o = new Character();

	$o->id			    =$row['id'];
	$o->id_game	  	=$row['id_game'];
	$o->name	    	=$row['name'];
	$o->cd_char 	  =$row['cd_char'];

	return $o;
}


?>
