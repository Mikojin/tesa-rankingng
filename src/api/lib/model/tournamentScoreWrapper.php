<?php
?>
<?php
/*****************************************************************************
 * tournament.php
 * Represente un Tournoi
 *****************************************************************************/



class TournamentScoreWrapper {

	// Game
  public $id_game;                  // id du jeu
	public $game_name;                // nom du jeu
	public $cd_game;                  // code du jeu

  // Tournament
  public $id_tournament;
	public $tournament_group_name;
	public $tournament_name;
	public $date_start;
	public $date_end;

  // scoring
	public $id_type_score;
	public $type_score;

  // info player
  public $id_player;
  public $ranking;
  public $score;

	function __construct() {
	}

}

function mapperTournamentScoreWrapper($row) {
  $o = new TournamentScoreWrapper();

  $o->id_game		  	          =$row['id_game'];
  $o->game_name	  	          =$row['game_name'];
  $o->cd_game		  	          =$row['cd_game'];
  $o->id_tournament           =$row['id_tournament'];
  $o->tournament_group_name   =$row['tournament_group_name'];
  $o->tournament_name         =$row['tournament_name'];
  $o->date_start		          =$row['date_start'];
  $o->date_end  		          =$row['date_end'];
  $o->id_type_score	          =$row['id_type_score'];
  $o->type_score  	          =$row['type_score_name'];
  $o->id_player  		          =$row['id_player'];
  $o->ranking   		          =$row['ranking'];
  $o->score     		          =$row['score'];

  return $o;
}

?>
