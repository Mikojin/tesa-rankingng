<?php
?>
<?php
/*****************************************************************************
 * season.php
 * Represente une saison pour les tournois.
 * Il s'agit d'un interval de date qui permet de regrouper tous les tournois
 * compris entre ces bornes pour définir un classement sur cet intervale.
 *****************************************************************************/



class Season {
	public $id;
	public $name;
	public $date_start;
	public $date_end;


	function __construct($id=null) {
		$this->id 	= $id;
	}
}



function mapperSeason($row) {
	$o = new Season();

	$o->id			=$row['id'];
	$o->name   		=$row['name'];
	$o->date_start	=$row['date_start'];
	$o->date_end	=$row['date_end'];

	return $o;
}


?>
