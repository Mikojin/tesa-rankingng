<?php
?>
<?php
/*****************************************************************************
 * participant.php
 * Represente un participant dans un tournois.
 * Dépend aussi de player.php
 *****************************************************************************/
require_once("./lib/model/player.php");

class Participant extends Player {
	public $id_player;
	public $id_tournament;
	public $ranking;

	// character info
	public $id_game;
	public $name;
	public $cd_char;

	function __construct($id_player=null) {
		$this->id_player 	= $id_player;
	}
}


function mapperParticipant($row) {
	$o = new Participant();

	$o = doMapperPlayer($o, $row);

	$o->id_player 	  	= $row['id_player'];
	$o->id_tournament 	= $row['id_tournament'];
	$o->ranking		    	= $row['ranking'];
	$o->id_game	    		= $row['id_game'];

	if(isset($row['cd_char'])) {
		$o->name		    	= $row['name'];
		$o->cd_char 		  = $row['cd_char'];
	}
	$o->score		      	= 0;
	if(isset($row['score'])) {
		$o->score	      	= $row['score'];
	}

	return $o;
}


?>
