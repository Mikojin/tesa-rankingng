<?php
?>
<?php
/*****************************************************************************
 * player.php
 * Represente un joueur
 *****************************************************************************/

class Player {
	public $id;
	public $pseudo;
	public $name;
	public $firstname;
	public $email;
	public $phone;
	public $status;
  public $cd_country;
  public $country;

	function __construct($id=null) {
		$this->id 	= $id;
	}
}


function doMapperPlayer($o, $row) {
	$o->id		  	  = $row['id'];
	$o->pseudo		  = $row['pseudo'];
	$o->name		    = $row['name'];
	$o->firstname	  = $row['firstname'];
	$o->email	  	  = $row['mail'];
	$o->phone	      = $row['phone'];
	$o->status		  = $row['status'];
	$o->cd_country  = $row['cd_country'];
	$o->country		  = $row['country'];

	return $o;
}

function mapperPlayer($row) {
	$o = new Player();

	$o = doMapperPlayer($o, $row);

	return $o;
}


?>
