<?php
?>
<?php
/*****************************************************************************
 * tournament.php
 * Represente un Tournoi
 *****************************************************************************/



class Tournament {
	public $id;

	public $id_game;
	public $game;

	public $id_type_score;
	public $type_score;

	public $group_name;
	public $name;
	public $date_start;
	public $date_end;

	function __construct($id=null) {
		$this->id 	= $id;
	}
}



function mapperTournament($row) {
	$o = new Tournament();

	$o->id		    		=$row['id'];
	$o->id_game		  	=$row['id_game'];
	$o->id_type_score	=$row['id_type_score'];
	$o->group_name		=$row['group_name'];
	$o->name	    		=$row['name'];
	$o->date_start		=$row['date_start'];
	$o->date_end  		=$row['date_end'];

	return $o;
}

?>
