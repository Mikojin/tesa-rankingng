<?php
?>
<?php
/*****************************************************************************
 * scoring.php
 * Represente une méthode de scoring d'un tournoi.
 * Ceci permet d'attribuer un nombre de point au joueur en fonction de son
 * classement final.
 *****************************************************************************/

class Scoring {
	public $id;
	public $id_type_score;
	public $rank_top;
	public $rank_bottom;
	public $score;

	function __construct($id_type_score=null, $rank_top=0, $rank_bottom=0, $score=0) {
		$this->id_type_score 	= $id_type_score;
		$this->rank_top		  	= $rank_top;
		$this->rank_bottom		= $rank_bottom;
		$this->score		    	= $score;
	}
}


function mapperScoring($row) {
	$o = new Scoring();

	$o->id			      	=$row['id'];
	$o->id_type_score   =$row['id_type_score'];
	$o->rank_top        =$row['rank_top'];
	$o->rank_bottom     =$row['rank_bottom'];
	$o->score           =$row['score'];

	return $o;
}

?>
