<?php
?>
<?php
/*****************************************************************************
 * playerGame.php
 * Represente une association jeu - joueur - personnage.
 * Permet de s'avoir quel est le personnage principalement joué pour ce joueur
 *****************************************************************************/

class PlayerGameWrapper {
	// Game
  public $id_game;                  // id du jeu
	public $game;                     // nom du jeu
	public $cd_game;                  // code du jeu

  // joueur
	public $id_player;                // id du joueur
	public $name;                      // nom du joueur
  public $firstname;                   // firstname du joueur
	public $pseudo;                   // pseudo du joueur

  // personnage
	public $id_char;                 // id du personnage
  public $char_name;                // nom du personnage
  public $cd_char;                  // le code du personnage (anciennement classe CSS)

	function __construct() {
	}
}


/**
 * mapper pour playerGameWrapper.
 * Données :
 * g.id as id_game, g.name as game, g.cd_game as cd_game,
 * p.id as id_player, p.name, p.firstname, p.pseudo,
 * c.id as id_char, c.name as char_name, c.cd_char as cd_char
 */
function mapperPlayerGameWrapper($row) {
	$o = new PlayerGameWrapper();
	$o->id_game		              = $row['id_game'];
	$o->game  		              = $row['game'];
	$o->cd_game		              = $row['cd_game'];

  $o->id_player	              = $row['id_player'];
	$o->name				              = $row['name'];
	$o->firstname		              = $row['firstname'];
	$o->pseudo		              = $row['pseudo'];

	$o->id_char                 = $row['id_char'];
	$o->char_name		            = $row['char_name'];
	$o->cd_char			            = $row['cd_char'];

	return $o;
}

?>
