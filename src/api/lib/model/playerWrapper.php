<?php
?>
<?php
/*****************************************************************************
 * player.php
 * Represente un le wrapper d'un joueur dans l'écran Joueur / ranking
 *****************************************************************************/

class PlayerWrapper {
  // player
	public $id;               // id du joueur
	public $pseudo;           // pseudo du joueur
	public $firstname;           // firstname du joueur
	public $name;              // nom du joueur


  // ranking
  public $rank_classe;      // class CSS du rang
	public $rank;             // Rang du joueur (numérique) (obsolète ? remplacé par current_rank)
	public $rank_display;     // affichage du rang : - si identique au précédent joueur
	public $current_rank;     // rang du joueur (identique à rank ?)
	public $previous_rank;    // rang précédent du joueur

  // character
  public $classe;           // classe css du personnage
  public $id_char;          // id du personnage
	public $character;        // nom du personnage
	public $cd_char;     // class css du personnage (TODO : à convertir en "nom du fichier" simplement. On impose une nommenclature)

  // game
	public $id_game;          // id du jeu
	public $game;             // nom du jeu
  public $cd_game;        // code du jeu

  // scoring
	public $points;           // score actuel du joueur sur la période
	public $previous_points;  // score précédent du joueur (score total sans prendre en compte le dernier tournois)
	public $new_points;       // nouveau score (obsolete ?)

	function __construct($id=null) {
		$this->id 	= $id;
	}
}

/***********************************************************************
 * Construit un Player à partir du resultat de la requete getInfoRanking
 */
function mapperPlayerWrapper($row) {
	$o = new PlayerWrapper();

	$o->id 			      	= $row['id'];
	$o->pseudo 		    	= $row['pseudo'];
	$o->name 	      		= $row['name'];
	$o->firstname 		 	= $row['firstname'];
	$o->points    			= $row['points'];
	$o->previous_points = $row['previous_points'];
	$o->new_points	  	= $row['new_points'];
	$o->character   		= $row['character'];
	$o->cd_char    		  = $row['cd_char'];
	$o->id_char 	    	= $row['id_char'];
	$o->game 		      	= $row['game'];
	$o->id_game     		= $row['id_game'];
  $o->cd_game         = $row['cd_game'];
	$o->current_rank  	= $row['current_rank'];
	$o->previous_rank 	= $row['previous_rank'];


	return $o;
}

?>
