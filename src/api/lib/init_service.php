<?php
require_once "./lib/lib_tools.php";
require_once "./lib/dao.php";

/***
 * Gestion du Cross-Origin Ressources Sharing
 * Permet d'appeler le service à partir d'un URL différente de celle du service.
 * A desactiver pour la prod
 */
cors();

function getInput() {

  // recuperation des inputs
  $input = file_get_contents("php://input");
  $r = null;
  if(isset($input) && !empty($input)) {
    $r  = json_decode($input);
  }
  return $r;
}

function callService(callable $service, $input) {
  $res = is_callable($service);

  if(!$res) {
    LibTools::log("call $service : not callable");
  }

  if($service) {
    LibTools::log("call $service");
    return $service($input);
  }
  return null;
}

function call() {
  $input = getInput();
  $result = [];
  if($input) {
    $serviceName = $input->f;
    $result = callService($serviceName, $input);
  } else {
    $result = callService("defaultService", $input);
  }

  $out = json_encode($result);

  if(isset($input->debug) && $input->debug == "true" ) {
    //LibTools::log("$serviceName size=".count($result));
    LibTools::log("json=".$out);
  }
  /*
  if(isset($serviceName) && $serviceName == "getCharacterList") {
    LibTools::log("getCharacterList size=".count($result));
    LibTools::log("json=".$out);
  }
  */
  if(!$out) {
    LibTools::log("json error=".json_last_error_msg());
  }

  //header('Content-Type: application/json; charset=UTF-8');
  echo $out;
}

?>
