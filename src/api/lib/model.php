<?php
?>
<?php

/*****************************************************************************
 * model.php
 * Regroupe les imports de toutes les classes de model.
 * Chaque classe de model contient aussi son mapper : base => objet.
 *****************************************************************************/

 /**
 * Import de chaque model
 */
require_once("./lib/model/character.php");
require_once("./lib/model/game.php");
require_once("./lib/model/participant.php");
require_once("./lib/model/player.php");
require_once("./lib/model/playerGameWrapper.php");
require_once("./lib/model/playerWrapper.php");
require_once("./lib/model/scoring.php");
require_once("./lib/model/season.php");
require_once("./lib/model/session.php"); // obsolete ?
require_once("./lib/model/tournament.php");
require_once("./lib/model/tournamentScoreWrapper.php");
require_once("./lib/model/user.php");


?>
