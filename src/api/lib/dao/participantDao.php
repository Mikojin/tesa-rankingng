<?php
?>
<?php
/*****************************************************************************
 * participantDao.php
 * DAO gérant les participants d'un tournoi
 *****************************************************************************/

require_once("./lib/lib_tools.php");
require_once("./lib/lib_dao.php");
require_once("./lib/model.php");

/*######################################################################
        Participant
 #######################################################################*/

class ParticipantDao extends AbstractDao {
	/***********************************************************************
	 * insert un nouveau participant
	 */
	function insert($id_tournament, $id_player, $ranking=0) {

		$sql = " insert into participant
		(id_tournament, id_player, ranking)
		values
		($id_tournament, $id_player, $ranking)";

		$this->exec_query($sql, "Insert Participant OK : $id_player");
	}

	/***********************************************************************
	 * sauvegarde le participant
	 */
	function save($id_tournament, $id_player, $ranking=0) {

		$sql = " update participant
		  set ranking       = $ranking
		where id_tournament = $id_tournament
		  and id_player     = $id_player";

		$this->exec_query($sql, "Save Participant OK : $id_tournament, $id_player, $ranking");
	}

	/***********************************************************************
	 * renvoie la liste personnage joué par les joueur du tournoi
	 */
	function getPlayerCharacterList($id_tournament) {
		$sql = "select pp.id_player, t.id_game, c.id as id_char, c.name, c.cd_char
			 from participant pp
			 join tournament t
			   on t.id = pp.id_tournament
			 join player p
			   on p.id = pp.id_player
			 join player_game pg
			   on pg.id_player		= pp.id_player
			  and pg.id_game		= t.id_game
			 join `character` c
			   on c.id 			= pg.id_character
			  and c.id_game		= pg.id_game
			where t.id 		 	= $id_tournament
			";

		LibTools::setLog("Participant.getPlayerCharacterList");
		$playerCharList = $this->fetch_map($sql, 'id_player');
		return $playerCharList;
	}

	/***********************************************************************
	 * supprime un participant
	 */
	function deleteParticipant($id_tournament, $id_player) {
		$sql = " delete from participant
		where id_tournament = $id_tournament
		  and id_player     = $id_player";

		$this->exec_query($sql, "delete Participant OK : $id_tournament, $id_player");
	}
}



?>
