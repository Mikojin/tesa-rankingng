<?php
?>
<?php
/*****************************************************************************
 * scoringDao.php
 * Table gérant une entrée d'un type score
 *****************************************************************************/

require_once("./lib/lib_tools.php");
require_once("./lib/lib_dao.php");
require_once("./lib/model.php");



/*######################################################################
        Scoring
 #######################################################################*/

class ScoringDao extends AbstractDao {
	/***********************************************************************
	 * insert un nouveau scoring
	 */
	function insert($id_type_score, $rank_top, $rank_bottom, $score) {

		$sql = " insert into scoring
		(id_type_score, rank_top, rank_bottom, score)
		values
		($id_type_score, $rank_top, $rank_bottom, $score)";

		$this->exec_query($sql, "Insert scoring OK : $id_type_score, $rank_top, $rank_bottom, $score");
	}

	/***********************************************************************
	 * Renvoie la liste complete des scoring pour le type_score donnée
	 * */
	function getList($id_type_score) {
		if(LibTools::isBlank($id_type_score)) {
			return array();
		}
		$sql = "select s.*
		from scoring s
		where s.id_type_score = $id_type_score
		order by s.rank_top asc, s.rank_bottom asc";

		$arr = $this->fetch_array($sql, 'mapperScoring');

		return $arr;
	}

	/***********************************************************************
	 * Renvoie le score dans le type_score pour le rank donné
	 * */
	function getScore($id_type_score, $rank) {
		$sql = "select s.*
		from `scoring` s
		where s.id_type_score = $id_type_score
		  and s.rank_top <= $rank
		  and s.rank_bottom >= $rank
		order by s.rank_top asc, s.rank_bottom asc";

		$scoring = $this->fetch_one($sql, "mapperScoring");

		return $scoring;
	}

	/***********************************************************************
	 * Renvoie la liste complete des type score
	 * */
	function getLastRank($idTypeScore) {
		$sql = "select max(rank_bottom) rank_last
		from `scoring`
		where id_type_score = $idTypeScore ";
		LibTools::setLog($sql);
		$row = $this->fetch_one($sql);
		if($row) {
			return $row['rank_last'];
		}
		return null;
	}



	/***********************************************************************
	 * supprimer la liste de scoring pour le id type score donné
	 */
	function deleteScoringList($id_type_score) {
		if(LibTools::isBlank($id_type_score)) {
			return false;
		}
		$sql = " delete from scoring
		where id_type_score=$id_type_score";

		return $this->exec_query($sql, "Delete scoring list OK : $id_type_score");
	}
}

?>
