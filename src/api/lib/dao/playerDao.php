<?php
?>
<?php
/*****************************************************************************
 * paramDao.php
 * Gestion des paramêtres globaux
 *****************************************************************************/

require_once("./lib/lib_tools.php");
require_once("./lib/lib_dao.php");
require_once("./lib/model.php");

/*######################################################################
        Player
 #######################################################################*/

class PlayerDao extends AbstractDao {

	/***********************************************************************
	 * Renvoie la map des parametre pour le groupe $group_name
	 * */
	function insert($pseudo, $firstname, $name, $mail, $phone ) {
		if(LibTools::isBlank($pseudo)) {
			LibTools::setLog("Insert New Player : Le pseudo est obligatoire");
			return;
		}
		if(LibTools::isBlank($firstname)) {
			LibTools::setLog("Insert New Player : Le firstname est obligatoire");
			return;
		}
		if(LibTools::isBlank($name)) {
			LibTools::setLog("Insert New Player : Le nom est obligatoire");
			return;
		}

		$mysqli = $this->open();

		$pseudo = stripslashes($pseudo	);
		$firstname = stripslashes($firstname	);
		$name 	= stripslashes($name		);
		$mail 	= stripslashes($mail	);
		$phone 	= stripslashes($phone		);
		$pseudo = $mysqli->real_escape_string($pseudo 	);
		$firstname = $mysqli->real_escape_string($firstname 	);
		$name 	= $mysqli->real_escape_string($name 	);
		$mail 	= $mysqli->real_escape_string($mail 	);
		$phone 	= $mysqli->real_escape_string($phone 	);

		$sql = "insert into player
		(pseudo, firstname, name, mail, phone)
		values
		('$pseudo', '$firstname', '$name', '$mail', '$phone')
		";
		$result = $mysqli->query($sql);
		$out = $this->check($result, $mysqli, "insert new player OK : ps=$pseudo, pr=$firstname, n=$name, m=$mail, t=$phone");
		$this->close($mysqli);
		return $out;
	}

	function save($id, $pseudo, $firstname, $name, $mail, $phone ) {
		if(LibTools::isBlank($id)){
			LibTools::setLog("Save Player : id obligatoire");
			return;
		}
		if(LibTools::isBlank($pseudo)) {
			LibTools::setLog("Save Player : Le pseudo est obligatoire");
			return;
		}
		if(LibTools::isBlank($firstname)) {
			LibTools::setLog("Save Player : Le firstname est obligatoire");
			return;
		}
		if(LibTools::isBlank($name)) {
			LibTools::setLog("Save Player : Le nom est obligatoire");
			return;
		}

		$mysqli = $this->open();

		$pseudo = stripslashes($pseudo	);
		$firstname = stripslashes($firstname	);
		$name 	= stripslashes($name		);
		$mail 	= stripslashes($mail	);
		$phone 	= stripslashes($phone		);
		$pseudo = $mysqli->real_escape_string($pseudo 	);
		$firstname = $mysqli->real_escape_string($firstname 	);
		$name 	= $mysqli->real_escape_string($name 	);
		$mail 	= $mysqli->real_escape_string($mail 	);
		$phone 	= $mysqli->real_escape_string($phone 	);

		// LibTools::setLog("Save Player TEST : $id, $pseudo, $firstname, $name, $mail, $phone");
		// return;

		$sql = "update player
		set pseudo='$pseudo', firstname='$firstname', name='$name', mail='$mail', phone='$phone'
		where id=$id
		";
		$result = $mysqli->query($sql);
		$out = $this->check($result, $mysqli, "save player OK : id=$id, ps=$pseudo, pr=$firstname, n=$name, m=$mail, t=$phone");
		$this->close($mysqli);
		return $out;
	}

	/***********************************************************************
	 * bascule le status du joueur entre masqué et affiché
	 * */
	function deletePlayer($id) {
		$sql = "delete from player
			where id=$id
		";
		return $this->exec_query($sql, "delete player OK : $id");
	}

	/***********************************************************************
	 * bascule le status du joueur entre masqué et affiché
	 * */
	function toggleStatus($id) {
		$sql = "update player
		set `status` = CASE `status`
			WHEN 'H' then NULL
			ELSE 'H'
			END
		where id=$id
		";
		return $this->exec_query($sql, "toggle status player OK : $id");
	}

	/***********************************************************************
	 * Renvoie le joueur donné
	 * */
	function get($id_player) {
		$sql = "select p.*, c.cd_country as cd_country, c.name as country
		from player p
    left outer join country c
      on c.id = p.id_country
		where p.id = $id_player";

		$player = $this->fetch_one($sql, 'mapperPlayer');

		return $player;
	}

	/***********************************************************************
	 * Renvoie la liste de tous les joueurs
	 * */
	function getListAll($idGame = null) {
		$sql = "select p.*, c.cd_country as cd_country, c.name as country
		from player p
    left outer join country c
      on c.id = p.id_country
		";
		if(isset($idGame) && $idGame != '') {
			$sql .= "join player_game pg
			 on pg.id_player = p.id
			 where pg.id_game = $idGame
			 ";
		}
		$sql .= "order by p.pseudo, p.firstname, p.name ";

		$playerList = $this->fetch_map($sql, 'id', 'mapperPlayer');

		return $playerList;
	}

	/***********************************************************************
	 * Renvoie la liste de tous les joueurs actif
	 * */
	function getList($idGame = null) {
		$sql = "select p.*, c.cd_country as cd_country, c.name as country
		from player p
    left outer join country c
      on c.id = p.id_country
		";
		if(isset($idGame) && $idGame != '') {
			$sql .= "join player_game pg
			 on pg.id_player = p.id
			 where pg.id_game = $idGame
			   and p.status is null
			 ";
		} else {
			$sql .= "where p.status is null
			";
		}
		$sql .= "order by p.pseudo, p.firstname, p.name ";

		$playerList = $this->fetch_array($sql, 'mapperPlayer');

		return $playerList;
	}

	/***********************************************************************
	 * Renvoie la liste des joueurs disponible pour le tournois
	 * */
	function getListTournament($idTournament) {
		$sql = "select p.*
		from player p
		left outer join participant pp
		  on pp.id_tournament = $idTournament
		 and pp.id_player = p.id
		where p.status is null
		and pp.id_player is null
		order by p.pseudo, p.firstname, p.name ";

		$playerList = $this->fetch_map($sql, 'id', 'mapperPlayer');

		return $playerList;
	}

	/***********************************************************************
	 * Renvoie la liste des résultats de tournois pour lequel le joueur a participé
	 * */
	function getParticipationList($id_player, $id_game=null, $date_start=null, $date_end=null) {
    if(!isset($id_player)) {
      return [];
    }
		$sql = "select g.id id_game, g.name game_name, g.cd_game, ts.*
		from tournament_score ts
    join game g
      on g.id = ts.id_game
    ";

		$sql .="where ts.id_player = $id_player
    ";

    if(isset($id_game)) {
      $sql .= "and ts.id_game = $id_game
      ";
    }

    if(isset($date_start)) {
      $sql .= "and ts.date_start >= '$date_start'
      ";
    }

    if(isset($date_end)) {
      $sql .= "and ts.date_start <= '$date_end'
      ";
    }
		$sql .="order by ts.date_start desc ";

		$participationList = $this->fetch_array($sql, 'mapperTournamentScoreWrapper');
		return $participationList;
	}

	/***********************************************************************
	 * Renvoie la liste des joueurs ne se trouvant pas dans le ranking du jeu
	 * */
	function getListNotRanked($id_game) {
		$sql = "select p.*
		from player p
		left outer join ranking r
		  on r.id_player = p.id
		 and r.id_game = $id_game
		where r.id_player is null
		  and p.status is null
		order by p.pseudo, p.firstname, p.name ";

		$playerList = $this->fetch_map($sql, 'id');

		return $playerList;
	}
}

?>
