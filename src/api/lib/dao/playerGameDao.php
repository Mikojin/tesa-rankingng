<?php
?>
<?php
/*****************************************************************************
 * playerGameDao.php
 * Table associant Player et Game pour savoir à quel jeu joue le joueur et
 * quel est son personnage principal
 *****************************************************************************/

require_once("./lib/lib_tools.php");
require_once("./lib/lib_dao.php");
require_once("./lib/model.php");


/*######################################################################
        Player => Game assoc
 #######################################################################*/

class PlayerGameDao extends AbstractDao {

	/***********************************************************************
	 * Renvoie la liste des joueurs ne se trouvant pas dans le ranking du jeu
	 * */
	function getList($id_player) {
		$sql = "select
      g.id as id_game, g.name as game, g.cd_game as cd_game,
      p.id as id_player, p.name, p.firstname, p.pseudo,
      c.id as id_char, c.name as char_name, c.cd_char as cd_char
		from player_game pg
    join player p
      on p.id = pg.id_player
		join game g
		  on g.id = pg.id_game
    join `character` c
      on c.id = pg.id_character
		where pg.id_player = $id_player
		order by g.name";

		$gameList = $this->fetch_array($sql, 'mapperPlayerGameWrapper');

		return $gameList;
	}

	function remove($id_player, $id_game) {
		$sql = "delete from player_game
		where id_player = $id_player
		  and id_game 	= $id_game";

		$out = $this->exec_query($sql, "delete player game OK : g=$id_game, p=$id_player");
		return $out;
	}

	function insert($id_player, $id_game, $id_char=null) {
		$sql = " insert into player_game
		(id_player, id_game, id_character)
		values
		($id_player, $id_game, $id_char)";

		$this->exec_query($sql, "Insert Player Game OK : $id_player, $id_game, $id_char");
	}

	/***********************************************************************
	 * sauvegarde le personnage joué par le joueur
	 */
	function save($id_player, $id_game, $id_character) {

		$sql = " update player_game
		  set id_character  = $id_character
		where id_player     = $id_player
		  and id_game       = $id_game";

		$this->exec_query($sql, "Save Player Game OK : $id_player, $id_game, $id_character");
	}

}


?>
