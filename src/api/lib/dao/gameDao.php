<?php
?>
<?php
/*****************************************************************************
 * _templateDao.php
 * base d'un DAO
 *****************************************************************************/

require_once("./lib/lib_tools.php");
require_once("./lib/lib_dao.php");
require_once("./lib/model.php");

/*######################################################################
        Game DAO : accès à la table Game
 #######################################################################*/

class GameDao extends AbstractDao {
	/***********************************************************************
	 * Renvoie la liste complete des jeux
	 * */
	function getList() {
		$sql = "select g.*
		from `game` g
    order by g.name ";

		$arr = $this->fetch_array($sql, 'mapperGame');

		return $arr;
	}
}

?>
