<?php
?>
<?php
/*****************************************************************************
 * typeScoreDao.php
 * DAO de la table TypeScore qui définie une façon de gérer le scoring d'un tournois
 *****************************************************************************/

require_once("./lib/lib_tools.php");
require_once("./lib/lib_dao.php");
require_once("./lib/model.php");

/*######################################################################
        Type Score
 #######################################################################*/

class TypeScoreDao extends AbstractDao {

	/***********************************************************************
	 * insert un nouveau type score
	 */
	function insert($type_name) {
		$mysqli = $this->open();
		$type_name = stripslashes($type_name);
		$type_name = $mysqli->real_escape_string($type_name);
		$this->close($mysqli);

		$sql = " insert into type_score
		(type_name)
		values
		('$type_name')";

		$ret = $this->exec_query($sql, "Insert type_score OK : $type_name");
		if(!$ret) {
			return false;
		}
		$sql = "select max(id) new_id from type_score";
		$row = $this->fetch_one($sql);
		if($row) {
			return $row['new_id'];
		}
		return false;
	}


	/***********************************************************************
	 * Renvoie la liste complete des type score
	 * */
	function getList() {
		$sql = "select ts.*
		from `type_score` ts
		order by ts.type_name ";

		LibTools::setLog("Tournament.getList");
		$arr = $this->fetch_map($sql, 'id');
		return $arr;
	}

	/***********************************************************************
	 * supprime le type score pour l'ID donné
	 */
	function deleteTypeScore($id) {
		$sql = " delete from type_score
		where id=$id";

		return $this->exec_query($sql, "Delete type_score OK : $id");
	}

}


?>
