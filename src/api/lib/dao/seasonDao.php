<?php
?>
<?php
/*****************************************************************************
 * seasonDao.php
 * DAO de la table Season
 *****************************************************************************/

require_once("./lib/lib_tools.php");
require_once("./lib/lib_dao.php");
require_once("./lib/model.php");

/*######################################################################
        Season
 #######################################################################*/

class SeasonDao extends AbstractDao {
	/***********************************************************************
	 * insert un nouveau scoring
	 */
	function insert($name, $date_start, $date_end) {

		$sql = " insert into season
		(`name`, date_start, date_end)
		values
		('$name', '$date_start', '$date_end')";

		$this->exec_query($sql, "Insert season OK : $name, $date_start, $date_end");
	}

	/***********************************************************************
	 * Renvoie la liste complete des seasons
	 * */
	function getList() {
		$sql = "select *
		from season
		order by date_end desc, date_start asc";

		$arr = $this->fetch_array($sql, 'mapperSeason');

		return $arr;
	}


	/***********************************************************************
	 * supprimer la liste de scoring pour le id type score donné
	 */
	function deleteSeason($id) {
		if(LibTools::isBlank($id)) {
			return false;
		}
		$sql = " delete from season
		where id=$id";

		return $this->exec_query($sql, "Delete season OK : $id");
	}
}

?>
