<?php
?>
<?php
/*****************************************************************************
 * characterDao.php
 * DAO de la table Character représentant un personnage pour un jeu
 *****************************************************************************/

require_once("./lib/lib_tools.php");
require_once("./lib/lib_dao.php");
require_once("./lib/model.php");

/*######################################################################
        Character
 #######################################################################*/

class CharacterDao extends AbstractDao {

	/***********************************************************************
	 * Renvoie la liste complete des joueurs
	 * */
	function getList($id_game) {
		$sql = "select *
		from `character` c
		where c.id_game = $id_game
		order by c.name ";

		LibTools::setLog("CharacterDao.getList idGame=".$id_game);
		$arr = $this->fetch_array($sql, 'mapperCharacter');

		return $arr;
	}
}

?>
