<?php
?>
<?php
/*****************************************************************************
 * userDao.php
 * DAO de la table User représentant un utilisateur avec ses droits.
 *****************************************************************************/

require_once("./lib/lib_tools.php");
require_once("./lib/lib_dao.php");
require_once("./lib/model.php");

/*######################################################################
        User
 #######################################################################*/

class UserDao extends AbstractDao {
	/***********************************************************************
	 * Renvoie un tablea contenant les user vérifier les infos données
	 * $username name de l'utilisateur
	 * $password le mot de passe associé
	 * */
	function get($username, $password) {
		$mysqli = $this->open();
		$username = stripslashes($username);
		$password = stripslashes($password);
		$username = $mysqli->real_escape_string($username);
		$password = $mysqli->real_escape_string($password);
		$this->close($mysqli);

		$sql = "select u.login, u.password, u.right
			from user u
			where u.login   ='$username'
			  and u.password='$password'";

		$arr = $this->fetch_array($sql);
		return $arr;
	}
}



?>
