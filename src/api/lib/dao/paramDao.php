<?php
?>
<?php
/*****************************************************************************
 * paramDao.php
 * Gestion des paramêtres globaux
 *****************************************************************************/

require_once("./lib/lib_tools.php");
require_once("./lib/lib_dao.php");
require_once("./lib/model.php");


/*######################################################################
        Param
 #######################################################################*/

class ParamDao extends AbstractDao {
	/***********************************************************************
	 * Renvoie la map des parametres pour le groupe $group_name
	 * */
	function loadGroup($group_name) {
		$sql = "
			select p.*
			  from param p
			 where p.group_name = '$group_name'
		";
		$map = $this->fetch_map($sql, 'variable');
		return $map;
	}

	/***********************************************************************
	 * Renvoie la valeur du parametre pour le groupe et le nom de variable donnée
	 * */
	function load($group_name, $variable) {
		$sql = "
			select p.*
			  from param p
			 where p.group_name = '$group_name'
			   and p.variable   = '$variable'
		";
		LibTools::setLog("load Param : $group_name ; $variable");
		$map = $this->fetch_one($sql);
		return $map['value'];
	}

	/***********************************************************************
	 * sauvegarde le parametre donnée.
	 * $group_name nom du groupe auquel appartient le parametre
	 * $var nom du parametre
	 * $value valeur du parametre
	 * */
	function save($group_name, $var, $value) {
		$sql = "
			update param
			  set value = '$value'
			where group_name = '$group_name'
			  and variable = '$var'
		";

		return $this->exec_query($sql, "save param OK : $group_name, $var=$value");
	}
}


?>

