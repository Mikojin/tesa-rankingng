<?php
?>
<?php
/*****************************************************************************
 * rankingDao.php
 * gère le ranking des joueurs
 *****************************************************************************/

require_once("./lib/lib_tools.php");
require_once("./lib/lib_dao.php");
require_once("./lib/model.php");

/*######################################################################
        Param
 #######################################################################*/

class RankingDao extends AbstractDao {

	/***********************************************************************
	 * renvoie le classement pour le tournoi d'id donné
	 */
	function getTournamentRanking($id_tournament) {
//		$sql = "select t.id_game, pp.*, p.*, s.score
		$sql = "select
          p.id, p.pseudo, p.name, p.firstname,
          g.id as id_game, g.name as game, g.cd_game as cd_game,
          c.id as id_char, c.name as `character`, c.cd_char as cd_char,
          ts.score as points, 0 as new_points, ts.score as previous_points,
          ts.ranking as current_rank, ts.ranking as previous_rank
        from tournament_score ts
        join game g
          on g.id = ts.id_game
        join player p
          on p.id = ts.id_player
        left outer join player_game pg
          on pg.id_player	= ts.id_player
          and pg.id_game	= ts.id_game
        left outer join `character` c
          on c.id 			= pg.id_character
			where ts.id_tournament 	= $id_tournament
			order by ts.ranking, p.pseudo, p.firstname, p.name  asc ";

		LibTools::setLog("Participant.getList");
		$participantList = $this->fetch_array($sql, 'mapperPlayerWrapper');
		return $participantList;
	}



	/***********************************************************************
	 *  Renvoie le classement des joueurs pour le jeu et l'interval de date donéne
	 */
	function getGlobalRanking($id_game, $date_min, $date_max) {
		$mysqli = $this->open();
		$sql="set @current_rank:=0";
		$result = $mysqli->query($sql);
		$out = $this->check($result, $mysqli, "ini @variable OK");
		$sql="set @previous_rank:=0";
		$result = $mysqli->query($sql);
		$out = $this->check($result, $mysqli, "ini @variable OK");
		$sql="set @current_rank_display:=0";
		$result = $mysqli->query($sql);
		$out = $this->check($result, $mysqli, "ini @variable OK");
		$sql="set @previous_rank_display:=0";
		$result = $mysqli->query($sql);
		$out = $this->check($result, $mysqli, "ini @variable OK");
		$sql="set @current_points:=0";
		$result = $mysqli->query($sql);
		$out = $this->check($result, $mysqli, "ini @variable OK");
		$sql="set @previous_points:=0";
		$result = $mysqli->query($sql);
		$out = $this->check($result, $mysqli, "ini @variable OK");


		$sql = "
select p.id, p.pseudo, p.name, p.firstname,
	crs.id_game, g.name as game, g.cd_game as cd_game,
	pg.id_character as id_char, c.name as `character`, c.cd_char as cd_char,
	crs.points, 0 as new_points, prs.previous_points,
	crs.current_rank, prs.previous_rank
from (
	select (@current_rank:=@current_rank + 1) as current_rank_true,
			if(@current_points = c2.points,
				@current_rank_display:=@current_rank_display,
				@current_rank_display:=@current_rank) as current_rank,
			(@current_points := c2.points) as current_points,
			c2.id_game, c2.id_player,
			c2.points
	from (
		select c.id_game, c.id_player,
			sum(c.score) as points
		from (
			select ts.id_game, ts.id_player, ts.score
			 from tournament_score ts
			where ts.date_start >= '$date_min'
			  and ts.date_start <= '$date_max'
		) c
		where c.id_game = $id_game
		group by c.id_game, c.id_player
		order by sum(c.score) desc
	) c2
) crs
left outer join (
	select (@previous_rank:=@previous_rank + 1) as previous_rank_true,
			if(@previous_points = pr2.previous_points,
				@previous_rank_display:=@previous_rank_display,
				@previous_rank_display:=@previous_rank) as previous_rank,
			(@previous_points := pr2.previous_points) as previous_points2,
			pr2.id_game, pr2.id_player,
			pr2.previous_points
	from (
		select  pr.id_game, pr.id_player, sum(pr.score) as previous_points
		from (
			select ts.id_game, ts.id_player, ts.score
			from tournament_score ts
			where ts.date_start < (
			select
				max(tt.date_start) last_date
			 from tournament tt
			where tt.date_start >= '$date_min'
			  and tt.date_start <= '$date_max'
			  and tt.id_game = ts.id_game
			)
		) pr
		where pr.id_game = $id_game
		group by pr.id_game, pr.id_player
		order by sum(pr.score) desc
	) pr2
) prs
   on prs.id_game 	= crs.id_game
  and prs.id_player = crs.id_player
 join player p
   on p.id 			= crs.id_player
  and p.status is null
 join game g
   on g.id 			= crs.id_game
 left outer join player_game pg
   on pg.id_player	= crs.id_player
  and pg.id_game	= crs.id_game
 left outer join `character` c
   on c.id 			= pg.id_character
where crs.id_game = $id_game
order by crs.points desc, crs.current_rank asc
		 ";
		// LibTools::setLog($sql);
		$rankingList = $this->fetch_array($sql, "mapperPlayerWrapper",$mysqli);
		$this->close($mysqli);
		return $rankingList;
	}

}
?>
