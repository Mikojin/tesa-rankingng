<?php
?>
<?php
/*****************************************************************************
 * tournamentDao.php
 * DAO pour les tables liées à la table Tournament
 *****************************************************************************/

require_once("./lib/lib_tools.php");
require_once("./lib/lib_dao.php");
require_once("./lib/model.php");

/*######################################################################
        Tournament
 #######################################################################*/

class TournamentDao extends AbstractDao {

	/***********************************************************************
	 * insert un nouveau tournoi
	 */
	function insert($id_game, $group_name, $name, $id_type_score, $date_start, $date_end) {
		if(LibTools::isBlank($group_name)) {
			LibTools::setLog("insert Tournament : group name is empty !!");
			return $g;
		}
		if(LibTools::isBlank($name)) {
			LibTools::setLog("insert Tournament : name is empty !!");
			return $g;
		}
		if(LibTools::isBlank($id_type_score)) {
			LibTools::setLog("insert Tournament : type score is empty !!");
			return $g;
		}
		$mysqli = $this->open();
		$group_name = stripslashes($group_name);
		$name 		= stripslashes($name);
		$date_start = stripslashes($date_start);
		$date_end 	= stripslashes($date_end);
		$group_name = $mysqli->real_escape_string($group_name);
		$name 		= $mysqli->real_escape_string($name);
		$date_start = $mysqli->real_escape_string($date_start);
		$date_end 	= $mysqli->real_escape_string($date_end);
		$this->close($mysqli);

		// LibTools::setLog("Insert tournament : id_game=$id_game, group_name=$group_name, name=$name, id_type_score=$id_type_score, date_start=$date_start, date_end=$date_end");
		$sql = " insert into tournament
		(id_game, group_name, `name`, id_type_score, date_start, date_end)
		values
		($id_game, '$group_name', '$name', $id_type_score, '$date_start', '$date_end')";

		return $this->exec_query($sql, "Insert tournament OK : id_game=$id_game, group_name=$group_name, name=$name, id_type_score=$id_type_score, date_start=$date_start, date_end=$date_end");
	}

	/***********************************************************************
	 * update le tournoi
	 */
	function save($id, $id_game, $group_name, $name, $id_type_score, $date_start, $date_end) {
		if(LibTools::isBlank($group_name)) {
			LibTools::setLog("update Tournament : group name is empty !!");
			return $g;
		}
		if(LibTools::isBlank($name)) {
			LibTools::setLog("update Tournament : name is empty !!");
			return $g;
		}
		if(LibTools::isBlank($id_type_score)) {
			LibTools::setLog("update Tournament : type score is empty !!");
			return $g;
		}
		$mysqli = $this->open();
		$group_name = stripslashes($group_name);
		$name 		= stripslashes($name);
		$date_start = stripslashes($date_start);
		$date_end 	= stripslashes($date_end);
		$group_name = $mysqli->real_escape_string($group_name);
		$name 		= $mysqli->real_escape_string($name);
		$date_start = $mysqli->real_escape_string($date_start);
		$date_end 	= $mysqli->real_escape_string($date_end);
		$this->close($mysqli);

		// LibTools::setLog("update tournament : id=$id, id_game=$id_game, group_name=$group_name, name=$name, id_type_score=$id_type_score, date_start=$date_start, date_end=$date_end");
		$sql = " update tournament
		set id_game=$id_game, group_name='$group_name', name='$name', id_type_score=$id_type_score, date_start='$date_start', date_end='$date_end'
		where id=$id
		";

		return $this->exec_query($sql, "update tournament OK :  id=$id, id_game=$id_game, group_name=$group_name, name=$name, id_type_score=$id_type_score, date_start=$date_start, date_end=$date_end");
	}


	/***********************************************************************
	 * Renvoie la liste de tout les tournois classé par date décroissante
	 */
	function getList($id_game, $date_start, $date_end) {
		$sql = "select *
		from tournament
		where id_game = $id_game
    ";

    if(isset($date_start) && $date_start != '') {
      $sql.=" and date_start >= '$date_start' ";
    }

    if(isset($date_end) && $date_end != '') {
      $sql.=" and date_start <= '$date_end' ";
    }
		$sql.=" order by date_start desc";

		LibTools::setLog("Tournament.getList $id_game $date_start $date_end");
		$arr = $this->fetch_array($sql, "mapperTournament");
		return $arr;
	}

	/***********************************************************************
	 * Renvoie le tournois pour l'id donné
	 */
	function get($id) {
		$sql = "select *
		from tournament
		where id = $id";

		$tournament = $this->fetch_one($sql, "mapperTournament");

		return $tournament;
	}

	/***********************************************************************
	 * supprime le tournament pour l'ID donné
	 */
	function deleteTournament($id) {
		$sql = " delete from tournament
		where id=$id";

		return $this->exec_query($sql, "Delete tournament OK : $id");
	}

}



?>
