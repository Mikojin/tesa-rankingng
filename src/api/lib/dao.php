<?php
?>
<?php
require_once("./lib/lib_tools.php");
require_once("./lib/lib_dao.php");
// require_once("./lib/mapper.php");
require_once("./lib/model.php");

/*****************************************************************************
 * dao.php
 * Fichier comprenant les requetes d'accès à la base de donnée.
 * Select, Update, Insert etc...
 * Regroupe les imports des dao individuel.
 *****************************************************************************/

// DAO
require_once("./lib/dao/characterDao.php");
require_once("./lib/dao/gameDao.php");
require_once("./lib/dao/paramDao.php");
require_once("./lib/dao/playerDao.php");
require_once("./lib/dao/playerGameDao.php");
require_once("./lib/dao/participantDao.php");
require_once("./lib/dao/rankingDao.php");
require_once("./lib/dao/scoringDao.php");
require_once("./lib/dao/seasonDao.php");
require_once("./lib/dao/tournamentDao.php");
require_once("./lib/dao/typeScoreDao.php");
require_once("./lib/dao/userDao.php");



/*######################################################################
        Dao Bundle
 #######################################################################*/

class Dao {
	public $characterDao;
	public $gameDao;
	public $histoRankingDao;
	public $paramDao;
	public $participantDao;
	public $playerDao;
	public $playerGameDao;
	public $rankingDao;
	public $scoringDao;
	public $seasonDao;
	public $typeScoreDao;
	public $tournamentDao;
	public $userDao;
	public $otherDao;

	function __construct() {
		$this->characterDao 	    = new CharacterDao();
		$this->gameDao 				    = new GameDao();
		$this->paramDao			      = new ParamDao();
		$this->participantDao	    = new ParticipantDao();
		$this->playerDao 			    = new PlayerDao();
		$this->playerGameDao 	    = new PlayerGameDao();
		$this->rankingDao			    = new RankingDao();
		$this->scoringDao		      = new ScoringDao();
		$this->seasonDao		      = new SeasonDao();
		$this->typeScoreDao		    = new TypeScoreDao();
		$this->tournamentDao	    = new TournamentDao();
		$this->userDao				    = new UserDao();

	}
}


?>

