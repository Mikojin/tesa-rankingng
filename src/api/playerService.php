<?php

/***********************************************************************
 * Service backend PHP POST.
 * Input et Output en JSON
 */

require_once "./lib/init_service.php";


/************************************************************************/

/**
 * Renvoie la liste des joueurs pour le jeu donnée
 * id_game :  l'id du jeu pour lequel on cherche la liste de joueur. Si null alors on renvoie pour tout les jeux
 */
function getPlayerList($input) {
  $dao = new Dao();

  $id_game = null;
  if(isset($input) && isset($input->id_game)) {
    $id_game = $input->id_game;
  }

	$output = $dao->playerDao->getList($id_game);

	return $output;
}

/**
 * Renvoie un joueur à partir de son id
 */
function getPlayer($input) {
  $dao = new Dao();
  $id_player = $input->id_player;

	$output = $dao->playerDao->get($id_player);

  return $output;
}

/**
 * Renvoie la liste de PlayerGameWrapper pour ce joueur.
 * Contient la liste des jeux et personnages princiapaux associés pour ce joueur.
 */
function getPlayerGameWrapperList($input) {
  $dao = new Dao();
  $id_player = $input->id_player;

	$output = $dao->playerGameDao->getList($id_player);

  return $output;
}


/**
 * Renvoie la liste des tournois auquel le joueur à participé
 * $id_player    : le joueur pour lequel on souhaite récupéré la liste
 * ($id_game)    : le jeu pour lequel on souhaite récupéré la liste
 * ($date_start) : la date à partir de laquelle on souhaite filtrer la liste
 * ($date_end)   : la date jusqu'à laquelle on souhaite filtrer la liste
 */
function getTournamentScoreWrapperList($input) {
  $dao = new Dao();
  $id_player = $input->id_player;

  $id_game = null;
  if(isset($input) && isset($input->id_game)) {
    $id_game = $input->id_game;
  }

  $date_start = null;
  if(isset($input) && isset($input->date_start)) {
    $date_start = $input->date_start;
  }

  $date_end = null;
  if(isset($input) && isset($input->date_end)) {
    $date_end = $input->date_end;
  }

  $output = $dao->playerDao->getParticipationList($id_player, $id_game, $date_start, $date_end);

  return $output;
}

function defaultService() {
  return getPlayerList(null);
}

call();

exit;
?>
