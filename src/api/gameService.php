<?php

/***********************************************************************
 * Service backend PHP POST.
 * Input et Output en JSON
 */

require_once "./lib/init_service.php";


/***********************************************************************
 * Renvoie la liste de Game
 * */
function getGameList($input) {
  $dao = new Dao();

  // recuperation du ranking pour la saison
	$output = $dao->gameDao->getList();

	return $output;
}

function defaultService() {
  return getGameList(null);
}

call();

exit;
?>
