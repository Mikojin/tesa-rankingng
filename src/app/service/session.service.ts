import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Observable, of } from 'rxjs';
import { Character } from '../model/character';
import { Game } from '../model/game';
import { Player } from '../model/player';
import { Season } from '../model/season';
import { Tournament } from '../model/tournament';
import { User } from '../model/user';
import { Logger } from '../tools/logger';
import { RankingService } from './ranking.service';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  private user:User;

  public static CD_CHAR_UNKNOWN="unknown";
  public static CHAR_UNKNOWN="Unknown";

  public static CK_SelectedGame = "selectedGame";
  public static CK_SelectedSeason = "selectedSeason";

  public initialised:boolean = false;

  public gameList:Array<Game> = null;
  public gameMap:Map<string,Game>= null;

  private _selectedGame:Game = null;
  set selectedGame(g:Game) {
    this._selectedGame = g;
    this.cookieService.set(SessionService.CK_SelectedGame, g.cd_game);
    Logger.debug(this, SessionService.CK_SelectedGame+" = "+g.cd_game);
  }
  get selectedGame():Game {
    return this._selectedGame;
  }

  public seasonList:Array<Season> = null;
  private _selectedSeason:Season = null;
  set selectedSeason(s:Season) {
    this._selectedSeason = s;
    this.cookieService.set(SessionService.CK_SelectedSeason, ""+s.id);
    Logger.debug(this, SessionService.CK_SelectedSeason+" = "+s.id);
  }
  get selectedSeason():Season {
    return this._selectedSeason;
  }

  public selectedTournament:Tournament = null;
  public selectedPlayer:Player = null;

  constructor(
    private rankingService:RankingService,
    private cookieService:CookieService
  ) {

  }

  public async init() {
    Logger.trace(this, "onInit synchrone start");
    this.initialised = false;
    await this.loadGameList();
    await this.loadAllCharacterMap();
    await this.loadSeasonList();
    this.initialised = true;
    Logger.trace(this, "onInit synchrone end");
  }


  /**
   * Charge la liste de jeu
   * @returns charge la liste de jeu et renvoie un observable sur cette dernière
   */
  private async loadGameList() {
    try {
      this.gameList = await this.rankingService.getGameList().toPromise();
      Logger.debug(this, 'liste Game chargé', this.gameList);
      // construction de la map
      this.gameMap = new Map<string, Game>();
      this.gameList.map((g:Game) => {this.gameMap.set(g.cd_game, g)});
      Logger.debug(this, 'Map Game initialisé', this.gameMap);
      this.initSelectedGame();
    } catch(error) {
      Logger.fatal(this, 'Erreur lors du chargement de la liste de jeu', error);
      alert('Erreur lors du chargement de la liste de jeu');
    }
  }

  /**
   * Initialise le jeu sélectionné par défaut
   */
  private initSelectedGame() {
    if (this.cookieService.check(SessionService.CK_SelectedGame)) {
      let selectedGameCode: string = this.cookieService.get(SessionService.CK_SelectedGame);
      this.selectedGame = this.gameMap.get(selectedGameCode);
    } else {
      this.selectedGame = this.gameList[0];
    }
    Logger.trace(this, "jeu par défaut", this.selectedGame);
  }

  /**
   * Charge la map de personnage pour tous les jeu
   */
  private async loadAllCharacterMap() {
    if(this.gameList== null) {
      Logger.error(this, "liste de jeu non chargé, impossible de charger la liste de personnage");
      return;
    }
    for( let g of this.gameList) {
      await this.loadCharacterMap(g);
    }
  }


  /**
   * Charge la map de personnage pour le jeu donné
   * @param game le jeu pour le quel on souhaite charger la map de personnage
   */
  private async loadCharacterMap(game:Game) {
    if(game == null) {
      Logger.error(this, 'loadCharacterMap : game est obligatoire');
    }
    try {
      let characterList:Array<Character> = await this.rankingService.getCharacterList(game.id).toPromise();
      Logger.trace(this, 'liste character chargé pour '+game.name, characterList);
      // construction de la map
      let characterMap = new Map<number, Character>();
      characterList.map((c:Character) => {characterMap.set(c.id, c); c.game = game;} );
      game.characterMap = characterMap;
    } catch(error) {
      const msg = 'Erreur lors du chargement de la liste de personnage pour '+game.name;
      Logger.fatal(this, msg, error);
      alert(msg);
    }
  }

    /**
   * Charge la liste des saisons en synchrone (ajout de await)
   */
  private async loadSeasonList() {
    try {
      this.seasonList = await this.rankingService.getSeasonList().toPromise();
      Logger.debug(this, 'liste des saisons chargé', this.seasonList);

      this.initSelectedSeason();

      Logger.trace(this, "saison par défaut", this.selectedSeason);
    } catch(error) {
      Logger.fatal(this, 'Erreur lors du chargement de la liste des saison', error);
      alert('Erreur lors du chargement de la liste des saisons');
    }
  }


  /**
   * initialise la saison sélectionné par défaut
   */
  private initSelectedSeason() {
    if (this.cookieService.check(SessionService.CK_SelectedSeason)) {
      let selectedSeasonId: string = this.cookieService.get(SessionService.CK_SelectedSeason);
      this.selectedSeason = this.seasonList.find(e => "" + e.id == selectedSeasonId);
    } else {
      this.selectedSeason = this.seasonList[0];
    }
  }
}
