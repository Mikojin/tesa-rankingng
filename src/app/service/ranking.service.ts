import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Character } from '../model/character';
import { ClassementRowWrapper } from '../model/classement-row-wrapper';
import { Game } from '../model/game';
import { Player } from '../model/player';
import { PlayerGameWrapper } from '../model/player-game-wrapper';
import { Season } from '../model/season';
import { Tournament } from '../model/tournament';
import { TournamentScoreWrapper } from '../model/tournament-score-wrapper';
import { Logger } from '../tools/logger';

@Injectable({
  providedIn: 'root'
})
export class RankingService {
  public CHAR_UNKNOWN:string = "0unknown";

  constructor(
    private httpClient:HttpClient
  ) { }

  private rootDir:string=environment.apiUrl;


  /**
   * @returns la liste des jeux disponibles
   */
  public getGameList(): Observable<Array<Game>> {
    return this.httpClient.post<Array<Game>>(
      this.rootDir+'api/gameService.php',
      {"f":"getGameList"}
    );
  }


  /**
   * @param id_game l'id du jeu pour lequel on souhaite renvoyer la liste de personnage
   * @returns la liste de personnage lié au jeu d'id donné
   */
  public getCharacterList(id_game:number): Observable<Array<Character>> {
    Logger.trace(this, "getCharacterList : id_game= "+id_game);
    return this.httpClient.post<Array<Character>>(
      this.rootDir+'api/characterService.php', {
        "f":"getCharacterList",
        "id_game":id_game
      }
    );
  }

  /**
   *
   * @returns Renvoi la liste des saisons
   */
  public getSeasonList(): Observable<Array<Season>> {
    return this.httpClient.post<Array<Season>>(
      this.rootDir+'api/seasonService.php',
      {"f":"getSeasonList"}
    );
  }

  /**
   * @param id_game l'id du jeu pour lequel on souhaite renvoyer la liste de joueurs associés
   * @returns la liste de joueurs associés au jeu d'id donné
   */
  public getPlayerList(id_game:number): Observable<Array<Player>> {
    Logger.trace(this, "getPlayerList : id_game= "+id_game);
    return this.httpClient.post<Array<Player>>(
      this.rootDir+'api/playerService.php', {
        "f":"getPlayerList",
        "id_game":id_game
      }
    );
  }

  /**
   *
   * @param id_player l'id du joueur à récupérer
   * @returns renvoie le joueur pour l'id donné
   */
  public getPlayer(id_player:number): Observable<Player> {
    Logger.trace(this, "getPlayer : id_player= "+id_player);
    return this.httpClient.post<Player>(
      this.rootDir+'api/playerService.php', {
        "f":"getPlayer",
        "id_player":id_player
      }
    );

  }

  /**
   *
   * @param id_player l'id du joueur pour lequel on souhaite récupérer la liste de jeu / perso
   * @returns la liste des jeux et personnage principaux associé à ce joueur
   */
  public getPlayerGameWrapperList(id_player:number):Observable<Array<PlayerGameWrapper>> {
    Logger.trace(this, "getPlayerGameWrapperList : id_player= "+id_player);
    return this.httpClient.post<Array<PlayerGameWrapper>>(
      this.rootDir+'api/playerService.php', {
        "f":"getPlayerGameWrapperList",
        "id_player":id_player
      }
    );

  }

  /**
   *
   * @param id_player l'id du joueur pour lequel on souhaite récupérer la liste de jeu / perso
   * @returns la liste des jeux et personnage principaux associé à ce joueur
   */
  public getTournamentScoreWrapperList(id_player:number, id_game:number, date_start:string, date_end:string):Observable<Array<TournamentScoreWrapper>> {
    Logger.trace(this, "getTournamentScoreWrapperList : id_player= "+id_player
      +" ; id_game= "+id_game
      +" ; date_start= "+date_start
      +" ; date_end= "+date_end);
    return this.httpClient.post<Array<TournamentScoreWrapper>>(
      this.rootDir+'api/playerService.php', {
        "f":"getTournamentScoreWrapperList",
        //"debug":"true",
        "id_player":id_player,
        "id_game":id_game,
        "date_start":date_start,
        "date_end":date_end
      }
    );

  }

  /**
   *
   * @param id_game id du jeux pour lequel on veut la liste de tournois
   * @returns la liste du classement pour le jeu donné.
   */
  public getTournamentList(id_game:number, date_start:string, date_end:string): Observable<Array<Tournament>> {
    return this.httpClient.post<Array<Tournament>>(
      this.rootDir+'api/tournamentService.php',
      {
        "f":"getTournamentList",
        "id_game":id_game,
        "date_start":date_start,
        "date_end":date_end,
      }
    );
  }


  /**
   *
   * @param id_tournament id du tournois à récupérer
   * @returns le tournoi pour l'id donné
   */
  public getTournament(id_tournament:number):Observable<Tournament> {
    return this.httpClient.post<Tournament>(
      this.rootDir+'api/tournamentService.php',
      {
        "f":"getTournament",
        "id_tournament":id_tournament,
      }
    );

  }

  /**
   *
   * @param id_tournament
   * @returns la liste représentant le classement pour ce tournoi.
   */
  public getClassementTournois(id_tournament:number):Observable<Array<ClassementRowWrapper>> {
    return this.httpClient.post<Array<ClassementRowWrapper>>(
      this.rootDir+'api/rankingService.php',
      {
        "f":"getTournamentRanking",
        "id_tournament":id_tournament,
      }
    );
  }

  /**
   *
   * @param id_game id du jeux pour lequel on veut le classement
   * @returns la liste du classement pour le jeu donné.
   */
  public getRankingList(
          id_game:number,
          date_start:string,
          date_end:string,
        ): Observable<Array<ClassementRowWrapper>> {
    return this.httpClient.post<Array<ClassementRowWrapper>>(
      this.rootDir+'api/rankingService.php',
      {
        "f":"getRanking",
        "id_game":id_game,
        "date_start":date_start,
        "date_end":date_end,
      }
    );
  }



}
