import { HttpClient } from '@angular/common/http';
import { Pipe, PipeTransform } from '@angular/core';
import { stringify } from 'querystring';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Character } from '../model/character';
import { ClassementRowWrapper } from '../model/classement-row-wrapper';
import { SessionService } from '../service/session.service';
import { Logger } from '../tools/logger';

@Pipe({
  name: 'imgChar'
})
export class ImgCharPipe implements PipeTransform {

  private DEFAULT_GAME:string = "SFV";

  transform(value: Character|ClassementRowWrapper, ...args: unknown[]): string {


    let cd_char:string = null;
    let cd_game:string = null;

    Logger.trace(this, "character Pipe : ", value);

    if(value.game === null) {
      cd_game = this.DEFAULT_GAME;
    } else if(value.game instanceof String || typeof value.game == 'string') {
      value = <ClassementRowWrapper> value;
      cd_game = value.cd_game;
      Logger.trace(this, "type ClassementRowWrapper");
    } else {
      value = <Character> value;
      cd_game = value.game.cd_game;
      Logger.trace(this, "type Character");
    }

    cd_char = value.cd_char;

    if(cd_char == null) {
      cd_char = SessionService.CD_CHAR_UNKNOWN;
    }


    let imgPath = environment.webroot + '/assets/images/games/' + cd_game + "/char/";
    let imgFile = imgPath + cd_char+".png";

    /*
    if(cd_char == null) {
      imgFile = imgPath + SessionService.CD_CHAR_UNKNOWN + ".png";
    }
    */
    return imgFile;
  }

}
