import { Pipe, PipeTransform } from '@angular/core';
import { environment } from 'src/environments/environment';

@Pipe({
  name: 'img'
})
export class ImgPipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): unknown {
    return environment.webroot + '/assets/images/' + value;
  }

}
