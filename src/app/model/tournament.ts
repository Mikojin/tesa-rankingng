import { Game } from "./game";

export class Tournament {

  public id:number;

	public id_game:number;
	public game:Game;

	public id_type_score:number;
	public type_score:string;

	public group_name:string;
	public name:string;
	public date_start:string;
	public date_end:string;


}
