import { Game } from "./game";

export class Character {
  public id:number;

	public id_game:number;
	public game:Game;

	public name:string;
	public cd_char:string;

  public get cd_game():string {
    return this.game?.cd_game;
  }

}
