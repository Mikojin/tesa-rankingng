export class ClassementRowWrapper {
  public id:number;

  public pseudo:string;
  public name:string;
  public firstname:string;

  public points:number;
  public previous_points:number;
  public new_points:number;
  public current_rank:number;
  public previous_rank:number;

  public rank_classe:string;
  public rank:number;
  public rank_display:string;

  public id_char:number;
  public character:string;
  public cd_char:string;

  public id_game:number;
  public game:string;
  public cd_game:string;

}
