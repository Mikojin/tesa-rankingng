export class TournamentScoreWrapper {

  // Game
	public id_game:number;            // id du jeu
	public game_name:string;          // nom du jeu
	public cd_game:string;            // code du jeu

  // Tournament
  public id_tournament:number;

	public tournament_group_name:string;
	public tournament_name:string;
	public date_start:string;
	public date_end:string;

  // scoring
	public id_type_score:number;
	public type_score:string;

  // info player
  public id_player:number;
  public ranking:number;
  public score:number;

}
