export class Player {

	public id:number;

  public pseudo:string;
	public name:string;
	public firstname:string;

  public email:string;
	public phone:string;

  public cd_country:string;
  public country:string;

  public status:string;

}
