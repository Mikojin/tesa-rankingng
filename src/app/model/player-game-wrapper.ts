
/**
 * Représente un joueur associé à un jeu et au personnage joué.
 */
export class PlayerGameWrapper {

  // jeu
  public id_game:number;
  public game:string;
  public cd_game:string;

  // joueur
  public id_player:number;
  public name:string;
  public firstname:string;
  public pseudo:string;

  // personnage
  public id_char:number;
  public char_name:string;
  public cd_char:string;

}
