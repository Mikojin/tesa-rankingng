import { Character } from "./character";

export class Game {
  public id:number;
  public cd_game:string;
  public name:string;


  public characterMap:Map<number,Character>;
}
