import { NiveauLog } from "./niveau-log.enum";

export class Logger {

  /**
   * Niveau de Log :
   * 0 : Fatal
   * 1 : error
   * 2 : warning
   * 3 : info
   * 4 : debug
   * 5 : trace
   *
   **/
  private static _niveauLog:NiveauLog = NiveauLog.Info;

  public static setLog(niveau:NiveauLog) {
    this._niveauLog=niveau;
  }

  public static setFatal():void {
    this._niveauLog = NiveauLog.Fatal;
  }

  public static setError():void {
    this._niveauLog = NiveauLog.Error;
  }

  public static setWarning():void {
    this._niveauLog = NiveauLog.Warning;
  }

  public static setInfo():void {
    this._niveauLog = NiveauLog.Info;
  }

  public static setDebug():void {
    this._niveauLog = NiveauLog.Debug;
  }

  public static setTrace():void {
    this._niveauLog = NiveauLog.Trace;
  }

  public static log(niv:NiveauLog, prefix:string, caller:Object, msg:string, ...obj:any) {
    if(niv <= this._niveauLog ) {
      console.log(prefix+" > "+caller.constructor.name+" : "+msg);
      for( let o of obj) {
        console.log(o);
      }
    }
  }

  public static logObj(...obj:any) {
      for( let o of obj) {
        console.log(o);
      }
  }

  /**
   *
   * @param msg message à logger
   * @param obj liste d'objet à logger'
   */
  public static fatal(caller:Object, msg:string, ...obj:any) {
    Logger.log(NiveauLog.Fatal, "FATAL", caller, msg, ...obj);
  }

  public static error(caller:Object, msg:string, ...obj:any) {
    Logger.log(NiveauLog.Error, "ERROR", caller, msg, ...obj);
  }

  public static warn(caller:Object, msg:string, ...obj:any) {
    Logger.log(NiveauLog.Warning, "WARN ", caller, msg, ...obj);
  }

  public static info(caller:Object, msg:string, ...obj:any) {
    Logger.log(NiveauLog.Info, "INFO ", caller, msg, ...obj);
  }

  public static debug(caller:Object, msg:string, ...obj:any) {
    Logger.log(NiveauLog.Debug, "DEBUG", caller, msg, ...obj);
  }

  public static trace(caller:Object, msg:string, ...obj:any) {
    Logger.log(NiveauLog.Trace, "TRACE", caller, msg, ...obj);
  }
}
