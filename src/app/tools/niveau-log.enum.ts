/**
 * Niveau de Log :
 * 0 : fatal
 * 1 : error
 * 2 : warning
 * 3 : info
 * 4 : debug
 * 5 : trace
 *
 **/
export enum NiveauLog {

  Fatal     = 0,
  Error     = 1,
  Warning   = 2,
  Info      = 3,
  Debug     = 4,
  Trace     = 5,
}
