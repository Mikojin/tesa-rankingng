export class Tools {

  public static wait(ms:number):Promise<void> {
    let p =  new Promise<void>(resolve => setTimeout(resolve, ms));
    return p;
  }

}
