import { Component } from '@angular/core';
import { environment } from 'src/environments/environment';
import { SessionService } from './service/session.service';
import { Logger } from './tools/logger';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'tesa-ranking-ng';


  constructor(
    private sessionService:SessionService,
  ) { }

  ngOnInit(): void {
    Logger.setLog(environment.logLevel);

    this.sessionService.init();
  }
}
