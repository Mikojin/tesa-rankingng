import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionClassementComponent } from './section-classement.component';

describe('SectionClassementComponent', () => {
  let component: SectionClassementComponent;
  let fixture: ComponentFixture<SectionClassementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectionClassementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionClassementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
