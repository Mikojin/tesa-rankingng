import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionAgendaComponent } from './section-agenda.component';

describe('SectionAgendaComponent', () => {
  let component: SectionAgendaComponent;
  let fixture: ComponentFixture<SectionAgendaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectionAgendaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionAgendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
