import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-highlight',
  templateUrl: './highlight.component.html',
  styleUrls: ['./highlight.component.css']
})
export class HighlightComponent implements OnInit {

  constructor(
    private route:ActivatedRoute,
    private router:Router,
  ) { }

  ngOnInit(): void {
  }

  public next() {
        this.router.navigate(['news'], {relativeTo: this.route});
  }

  public onClick() {

  }

}
