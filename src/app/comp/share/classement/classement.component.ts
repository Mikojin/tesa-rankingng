import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ClassementRowWrapper } from 'src/app/model/classement-row-wrapper';
import { Game } from 'src/app/model/game';
import { Player } from 'src/app/model/player';
import { Season } from 'src/app/model/season';
import { Tournament } from 'src/app/model/tournament';
import { RankingService } from 'src/app/service/ranking.service';
import { SessionService } from 'src/app/service/session.service';
import { Logger } from 'src/app/tools/logger';
import { Tools } from 'src/app/tools/tools';

@Component({
  selector: 'app-classement',
  templateUrl: './classement.component.html',
  styleUrls: ['./classement.component.css']
})
export class ClassementComponent implements OnInit {

  constructor(
    private route:ActivatedRoute,
    private router:Router,
    private sessionService:SessionService,
    private rankingService:RankingService,
  ) { }


  public classementList:Array<ClassementRowWrapper>;

  @Input()
  get selectedGame():Game {
    return this._selectedGame;
  };
  set selectedGame(game:Game) {
    if(this._selectedGame?.id == game?.id) {
      return;
    }
    this._selectedGame = game;
    Logger.trace(this, "selectedGame changed : "+game?.name);
    this.loadClassementList();
  }
  private _selectedGame:Game = null;

  @Input()
  get selectedSeason():Season {
    return this._selectedSeason;
  }
  set selectedSeason(season:Season) {
    this._selectedSeason = season;
    Logger.trace(this, "selectedSeason changed : "+season.name);
    this.loadClassementList();
  }
  private _selectedSeason:Season = null;

  @Input()
  get selectedTournament():Tournament {
    return this._selectedTournament;
  }
  set selectedTournament(tournament:Tournament) {
    this._selectedTournament = tournament;
    Logger.trace(this, "selectedTournament changed : "+tournament.id);
    this.loadClassementList();
  }
  public _selectedTournament = null;

  ngOnInit(): void {
    this.onOpen();
  }

  private async onOpen() {
    // on s'assure que la session est initialisée
    while(!this.sessionService.initialised) {
      await Tools.wait(500);
    }
    this.loadClassementList();
  }



  //==========================================================
  //     Constant
  //==========================================================

  get cdCharUnknown():string {
    return SessionService.CD_CHAR_UNKNOWN;
  }

  get charUnknown():string {
    return SessionService.CHAR_UNKNOWN;
  }

  //==========================================================
  //     Action
  //==========================================================

  public onClickJoueur(player:Player):void {
    this.sessionService.selectedPlayer = player;
    this.router.navigate(['joueurs', player.id]);
  }

  //==========================================================
  //     Classement
  //==========================================================

  /**
   * Charge le classement
   */
  private loadClassementList():void {

    if(this.selectedTournament) {
      Logger.trace(this, "doLoadClassementTournois");
      this.doLoadClassementTournois();
    } else {
      Logger.trace(this, "doLoadClassementGlobal");
      this.doLoadClassementGlobal();
    }

  }

  private doLoadClassementTournois():void {
    let id_tournament:number     = null;
    if(this.selectedTournament) {
      id_tournament     = this.selectedTournament.id
    }
    if(!id_tournament) {
      Logger.trace(this, 'id_tournament non fourni');
      return;
    }
    this.rankingService.getClassementTournois(id_tournament).subscribe(result => {
      this.classementList = result;
      Logger.trace(this, 'liste ranking tournois chargé '+id_tournament, this.classementList);
    }, (error) => {
      Logger.fatal(this, 'Erreur lors du chargement du classement du tournois '+id_tournament, error);
      alert('Erreur lors du chargement du classement du tournois');
    });


  }


  private doLoadClassementGlobal():void {
    let idGame:number            = null;
    let date_start:string        = "";
    let date_end:string          = "";
    if(this.selectedGame) {
      idGame            = this.selectedGame.id;
    }
    if(this.selectedSeason) {
      date_start        = this.selectedSeason.date_start;
      date_end          = this.selectedSeason.date_end;
    }

    if(idGame == null) {
      Logger.trace(this, 'aucun jeu selectionné, chargement du classement impossible');
      return;
    }

    this.rankingService.getRankingList(idGame, date_start, date_end).subscribe(result => {
      this.classementList = result;
      Logger.trace(this, 'liste ranking chargé', this.classementList);
    }, (error) => {
      Logger.fatal(this, 'Erreur lors du chargement de la liste de classement', error);
      alert('Erreur lors du chargement de la liste de classement');
    });
  }

}
