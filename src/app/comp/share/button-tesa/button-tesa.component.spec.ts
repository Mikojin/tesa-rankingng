import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonTesaComponent } from './button-tesa.component';

describe('ButtonTesaComponent', () => {
  let component: ButtonTesaComponent;
  let fixture: ComponentFixture<ButtonTesaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ButtonTesaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonTesaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
