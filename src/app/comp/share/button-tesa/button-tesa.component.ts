import { MethodCall } from '@angular/compiler';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Logger } from 'src/app/tools/logger';

@Component({
  selector: 'app-button-tesa',
  templateUrl: './button-tesa.component.html',
  styleUrls: ['./button-tesa.component.css']
})
export class ButtonTesaComponent implements OnInit {

  constructor(
    private route:ActivatedRoute,
    private router:Router,
  ) { }

  @Input()
  public label:string;
  @Input()
  public url:string;

  @Input()
  public caller:Component;
  /**
   * La callback doit prendre un objet sur lequel porte la fonction et/ou les data en entrée
   */
  @Input()
  public callback:Function;
  @Input()
  public data:any;

  ngOnInit(): void {
  }

  public onClick():void {
    if(this.caller && this.callback) {
      // on appelle la callback via l'objet appelant : caller.callback(data)
      this.callback(this.caller, this.data);
    }
    if(this.callback){
      // si pas de caller, on appelle la callback directement avec les données
      // /!\ dans ce cas, si le code de la callback appelle "this" ça fera reférence à ButtonTesaComponent
      this.callback(this.data);
    } else if(this.url) {
      this.router.navigate([this.url]);
    } else {
      Logger.warn(this, "no URL nor callback define for this button");
    }
    return ;
  }

}
