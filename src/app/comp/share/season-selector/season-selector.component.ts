import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Season } from 'src/app/model/season';
import { SessionService } from 'src/app/service/session.service';
import { Logger } from 'src/app/tools/logger';
import { Tools } from 'src/app/tools/tools';

@Component({
  selector: 'app-season-selector',
  templateUrl: './season-selector.component.html',
  styleUrls: ['./season-selector.component.css']
})
export class SeasonSelectorComponent implements OnInit {

  constructor(
      private sessionService:SessionService,
  ) { }

  @Output()
  public onChangeEvent = new EventEmitter<Season>();

  @Input()
  public selectedSeason:Season = null;

  public seasonList:Array<Season>;


  ngOnInit(): void {
    this.onOpen();
  }

  private async onOpen() {
    // on s'assure que la session est initialisée
    while(!this.sessionService.initialised) {
      await Tools.wait(500);
    }
    this.initSeasonList();
  }

    //==========================================================
  //     Saisons
  //==========================================================

  /**
   * Initialisation de la liste des saisons et de la saison sélectionné par défaut
   */
  private initSeasonList():void {
      // chargement de la liste des season
      this.seasonList = this.sessionService.seasonList;
      this.selectedSeason = this.sessionService.selectedSeason;
      if(this.selectedSeason == null) {
        this.initSelectedSeason();
      }

  }

  /**
   * Initialisation de la saison sélectionné par défaut
   */
  private initSelectedSeason():void {
    if(this.selectedSeason != null) {
      return;
    }
    // on selectionne la premiere saison de la liste
    for(let s of this.seasonList) {
      this.selectedSeason = s;
      Logger.trace(this, "saison par défaut", this.selectedSeason);
      break;
    }
    this.sessionService.selectedSeason = this.selectedSeason;
  }


  public onChangeSeason():void {
    Logger.info(this, "jeu selectionné : "+this.selectedSeason.name);
    this.sessionService.selectedSeason = this.selectedSeason;
    this.onChangeEvent.emit(this.selectedSeason);
  }

}
