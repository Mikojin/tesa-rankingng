import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Game } from 'src/app/model/game';
import { SessionService } from 'src/app/service/session.service';
import { Logger } from 'src/app/tools/logger';
import { Tools } from 'src/app/tools/tools';

@Component({
  selector: 'app-game-selector',
  templateUrl: './game-selector.component.html',
  styleUrls: ['./game-selector.component.css']
})
export class GameSelectorComponent implements OnInit {

  constructor(
    private sessionService:SessionService,
  ) {
    this.DEFAULT_GAME.id = null;
    this.DEFAULT_GAME.cd_game = "";
    this.DEFAULT_GAME.name = "<Tous>";
  }

  protected DEFAULT_GAME:Game = new Game();

  @Input()
  public unselectedName:string = null;

  /**
   * Défini si la selection du jeu est persisté dans la session
   */
  @Input()
  public isSessionLinked:boolean = true;


  public gameList:Array<Game>;

  @Output()
  public onChangeEvent = new EventEmitter<Game>();

  @Input()
  public selectedGame:Game = null;


  ngOnInit(): void {
    this.onOpen();
  }

  private async onOpen() {
    // on s'assure que la session est initialisée
    while(!this.sessionService.initialised) {
      await Tools.wait(500);
    }
    this.initGameList();
  }

  private initGameList():void {
    this.gameList = Array.from(this.sessionService.gameList);
    if(this.unselectedName != null) {
      this.DEFAULT_GAME.name = this.unselectedName;
      this.gameList.unshift(this.DEFAULT_GAME);
    }
    Logger.debug(this, "liste jeux chargé : "+this.gameList?.length, this.gameList);
    if(this.selectedGame == null) {
      this.initSelectedGame();
    }
  }

  private initSelectedGame():void {
    if(this.isSessionLinked) {
      this.selectedGame = this.sessionService.selectedGame;
    }
    if(this.selectedGame == null ) {
      if(this.unselectedName != null) {
        this.selectedGame = this.DEFAULT_GAME;
      } else {
        // on selectionne le premier jeu de la liste
        for(let g of this.gameList) {
          this.selectedGame = g;
          Logger.trace(this, "jeu par défaut", this.selectedGame);
          break;
        }
      }
      if(this.isSessionLinked) {
        this.sessionService.selectedGame = this.selectedGame;
      }
    }
  }


  public onChangeGame():void {
    if(this.selectedGame != null) {
      Logger.info(this, "jeu selectionné : "+this.selectedGame.name);
      if(this.isSessionLinked) {
        this.sessionService.selectedGame = this.selectedGame;
      }
      this.onChangeEvent.emit(this.selectedGame);
    } else {
      Logger.info(this, "jeu déselectionné");
      this.onChangeEvent.emit(this.selectedGame);
    }
  }

}
