import { Component, OnInit } from '@angular/core';
import { AbstractComponent } from 'src/app/tools/abstract-component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent extends AbstractComponent implements OnInit {

  constructor() {
    super();
  }

  ngOnInit(): void {
  }

}
