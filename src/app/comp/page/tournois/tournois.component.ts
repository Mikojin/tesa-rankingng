import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Game } from 'src/app/model/game';
import { Season } from 'src/app/model/season';
import { Tournament } from 'src/app/model/tournament';
import { RankingService } from 'src/app/service/ranking.service';
import { SessionService } from 'src/app/service/session.service';
import { AbstractComponent } from 'src/app/tools/abstract-component';
import { Logger } from 'src/app/tools/logger';
import { Tools } from 'src/app/tools/tools';

@Component({
  selector: 'app-tournois',
  templateUrl: './tournois.component.html',
  styleUrls: ['./tournois.component.css']
})
export class TournoisComponent extends AbstractComponent implements OnInit {


  constructor(
    private route:ActivatedRoute,
    private router:Router,
    private sessionService:SessionService,
    private rankingService:RankingService,
  ) {
    super();
  }

  public gameList:Array<Game>;
  public seasonList:Array<Season>;
  public selectedGame:Game = null;
  public selectedSeason:Season = null;

  public tournamentList:Array<Tournament>;

  ngOnInit(): void {
    this.onOpen();
  }

  private async onOpen() {
    // on s'assure que la session est initialisée
    while(!this.sessionService.initialised) {
      await Tools.wait(500);
    }
    this.selectedGame = this.sessionService.selectedGame;
    this.selectedSeason = this.sessionService.selectedSeason;
    this.loadTournamentList();
  }

  //==========================================================
  //     Tournois
  //==========================================================

  /**
   * Charge le classement pour ce jeux
   */
  private loadTournamentList():void {
    let idGame            = 1;
    let date_start        = "2000-01-01";
    let date_end          = "2100-01-01";
    if(this.selectedGame) {
      idGame            = this.selectedGame.id;
      date_start        = this.selectedSeason.date_start;
      date_end          = this.selectedSeason.date_end;
    }

    this.rankingService.getTournamentList(idGame, date_start, date_end).subscribe(result => {
      this.tournamentList = result;
      Logger.trace(this, 'liste tournois chargé', this.tournamentList);
    }, (error) => {
      Logger.fatal(this, 'Erreur lors du chargement de la liste de tournois', error);
      alert('Erreur lors du chargement de la liste de tournois');
    });
  }



  //==========================================================
  //     On Change methods
  //==========================================================

  /**
   *
   * @param game le nouveau jeu selectionné
   */
  public onChangeGame(game:Game):void {
    this.selectedGame = game;
    Logger.trace(this, "jeu selectionné : "+this.selectedGame.name);
    this.loadTournamentList();
  }

  /**
   *
   * @param season la nouvelle saison sélec
   */
  public onChangeSeason(season:Season):void {
    this.selectedSeason = season;
    Logger.trace(this, "saison selectionnée : "+this.selectedSeason.name);
    this.loadTournamentList();
  }

  public onClickClassement(comp:TournoisComponent, tournament:Tournament):void {
    comp.sessionService.selectedTournament = tournament;
    comp.router.navigate(['/tournois/'+tournament.id]);

  }
}
