import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Game } from 'src/app/model/game';
import { Player } from 'src/app/model/player';
import { RankingService } from 'src/app/service/ranking.service';
import { SessionService } from 'src/app/service/session.service';
import { Logger } from 'src/app/tools/logger';
import { Tools } from 'src/app/tools/tools';

@Component({
  selector: 'app-joueurs',
  templateUrl: './joueurs.component.html',
  styleUrls: ['./joueurs.component.css']
})
export class JoueursComponent implements OnInit {

  constructor(
    private route:ActivatedRoute,
    private router:Router,
    private sessionService:SessionService,
    private rankingService:RankingService,
  ) { }

  public selectedGame:Game;
  public playerList:Array<Player>;
  public filteredPlayerList:Array<Player> = new Array();

  private _filter:string = "";
  get filter():string {
    return this._filter;
  }
  set filter(f:string) {
    this._filter = f;
    this.onChangeFilter(f);
  }

  ngOnInit(): void {
    this.onOpen();
  }

  private async onOpen() {
    // on s'assure que la session est initialisée
    while(!this.sessionService.initialised) {
      await Tools.wait(500);
    }
    this.selectedGame = this.sessionService.selectedGame;
    this.loadPlayerList();
  }

  private loadPlayerList():void {
    let id_game:number = this.selectedGame.id;
    if(id_game <0) {
      id_game = null;
    }
    this.rankingService.getPlayerList(id_game).subscribe(result => {
      this.playerList = result;
      Logger.trace(this, "loadPlayerList : chargé ="+this.playerList?.length, this.playerList);
      this.onChangeFilter(this.filter);
    }, (error) => {
      Logger.fatal(this, 'Erreur lors du chargement de la liste de joueurs', error);
      alert('Erreur lors du chargement de la liste de joueurs');
    });

  }

  /**
   * @param game le jeu selectionné
   */
  public onChangeGame(game:Game):void {
    this.selectedGame = game;
    Logger.trace(this, "jeu selectionné : "+this.selectedGame.name);
    this.loadPlayerList();
  }

  public onClickJoueur(player:Player):void {
    this.sessionService.selectedPlayer = player;
    this.router.navigate(['joueurs', player.id]);
  }

  public onChangeFilter(f:string) {
    Logger.trace(this, "onChangeFilter "+f);
    // vide le filtre
    this.filteredPlayerList.length = 0;
    if(this.filter == null || this.filter.trim() == "") {
      this.filteredPlayerList = Array.from(this.playerList);
    } else {
      const filtre = f.toLowerCase();
      for(let p of this.playerList) {
        this.filtrer(filtre, p);
      }
    }
  }



  private filtrer(filtre:string, player:Player) {
    if(player.pseudo.toLowerCase().includes(filtre) ||
      player.name.toLowerCase().includes(filtre) ||
      player.firstname.toLowerCase().includes(filtre)
    ) {
      this.filteredPlayerList.push(player);
    }
  }

}
