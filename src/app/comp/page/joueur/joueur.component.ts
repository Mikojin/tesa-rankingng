import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ClassementRowWrapper } from 'src/app/model/classement-row-wrapper';
import { Game } from 'src/app/model/game';
import { Player } from 'src/app/model/player';
import { PlayerGameWrapper } from 'src/app/model/player-game-wrapper';
import { Season } from 'src/app/model/season';
import { TournamentScoreWrapper } from 'src/app/model/tournament-score-wrapper';
import { RankingService } from 'src/app/service/ranking.service';
import { SessionService } from 'src/app/service/session.service';
import { Logger } from 'src/app/tools/logger';
import { Tools } from 'src/app/tools/tools';

@Component({
  selector: 'app-joueur',
  templateUrl: './joueur.component.html',
  styleUrls: ['./joueur.component.css']
})
export class JoueurComponent implements OnInit {

  constructor(
    private route:ActivatedRoute,
    private router:Router,
    private sessionService:SessionService,
    private rankingService:RankingService,
  ) { }

  private _player:Player = new Player();
  public get player():Player {
    return this._player;
  }
  public set player(p:Player) {
    this._player = p;
    Logger.debug(this, "joueur selectionné : "+p.pseudo, p);
    this.initPlayerGameWrapperList();
  }

  public playerGameWrapperList:Array<PlayerGameWrapper> = null;

  get selectedGame():Game {
    return this._selectedGame;
  };
  set selectedGame(game:Game) {
    if(this._selectedGame?.id == game?.id) {
      return;
    }
    this._selectedGame = game;
    Logger.trace(this, "selectedGame changed : "+game?.name);
    this.loadClassementList();
  }
  private _selectedGame:Game = null;

  get selectedSeason():Season {
    return this._selectedSeason;
  }
  set selectedSeason(season:Season) {
    this._selectedSeason = season;
    Logger.trace(this, "selectedSeason changed : "+season.name);
    this.loadClassementList();
  }
  private _selectedSeason:Season = null;

  public score_total:number = 0;

  /**
   * Représente la liste de tournois auquel le joueur à participé.
   * Filtrer par jeu et par saison.
   */
  public playerRankingList:Array<TournamentScoreWrapper>;

  ngOnInit(): void {
    Logger.trace(this, "ngOnInit");
    this.onOpen();
  }


  private async onOpen() {
    // on s'assure que la session est initialisée
    while(!this.sessionService.initialised) {
      await Tools.wait(500);
    }
    this.selectedGame = null;
    this.selectedSeason = this.sessionService.selectedSeason;
    this.initSelectedPlayer();
    this.loadClassementList();
  }

  //=======================================================
  // Player Info
  //=======================================================

  /**
   * initialise le joueur selectionné à partir de la session/URL.
   */
  private initSelectedPlayer():void {
    let pseudo:string = this.route.snapshot.paramMap.get('pseudo');
    let id_player = this.route.snapshot.params['id'];
    if(id_player != null) {
      Logger.trace(this, "acces par id : "+id_player);
      // id_player fourni
      if(this.sessionService.selectedPlayer && this.sessionService.selectedPlayer.id == id_player) {
        // si le joueur en session est le bon, alors on l'affecte directement.
        this.player = this.sessionService.selectedPlayer;
      } else {
        // sinon, on le recharge de la base
        this.loadPlayer(id_player);
      }
    } else {
      Logger.trace(this, "acces par pseudo : "+pseudo);
    }

  }

  /**
   * Charge les information générique du joueur
   * @param id_player l'id du joueur à charger
   */
  private loadPlayer(id_player:number):void {
    this.rankingService.getPlayer(id_player).subscribe(result => {
      this.player = result;
      // ajout en session du joueurs
      this.sessionService.selectedPlayer = this.player;
      Logger.trace(this, 'joueur chargé', this.player);
      this.loadClassementList();
    }, (error) => {
      Logger.fatal(this, 'Erreur lors du chargement du joueur', error);
      alert('Erreur lors du joueur');
    });
  }

  /**
   * Charge la listes de jeux joué par le joueur ainsi que le personnage joué
   */
  private initPlayerGameWrapperList():void {
    if(this.player == null) {
      return;
    }
    this.rankingService.getPlayerGameWrapperList(this.player.id).subscribe(result => {
      this.playerGameWrapperList = result;
      Logger.trace(this, 'playerGameWrapperList chargé pour '+this.player.pseudo, this.playerGameWrapperList);
    }, (error) => {
      Logger.fatal(this, 'Erreur lors du chargement de la liste des jeux associé au joueur', error);
      alert('Erreur lors du chargement de liste des jeux associé au joueur joueur');
    });

  }

  /**
   * Charge le classement pour ce joueur en fonction du jeu et de la saison sélectionné
   */
  private loadClassementList():void {
    this.score_total = 0;
    if(!this.player || !this.player.id) {
      // le joueur n'est pas encore chargé
      Logger.trace(this, 'playerRankingList id_player non initialisé ');
      return;
    }
    Logger.trace(this, 'playerRankingList : pseudo='+this.player.pseudo);

    let id_player = this.player ? this.player.id:null;
    let id_game = this.selectedGame ? this.selectedGame.id : null;

    this.rankingService.getTournamentScoreWrapperList(
      id_player, id_game,
      this.selectedSeason?.date_start,
      this.selectedSeason?.date_end).subscribe(result => {
        this.playerRankingList = result;
        this.updateScore();
        Logger.trace(this, 'playerRankingList chargé pour '+this.player.pseudo, this.playerRankingList);
      }, (error) => {
        Logger.fatal(this, 'Erreur lors du chargement de la liste des tournois associé à ce joueur', error);
        alert('Erreur lors du chargement de la liste des tournois associé au joueur');
      }
    );

  }

  public updateScore():void {
    this.score_total = 0;
    for(let p of this.playerRankingList) {
      this.score_total = +this.score_total + +p.score;
    }
  }

  /**
   *
   * @param game le nouveau jeu sélectionné
   */
  public onChangeGame(game:Game):void {
    this.selectedGame = game;
    Logger.trace(this, "jeu selectionné : "+this.selectedGame.name);
  }

  /**
   *
   * @param season la nouvelle saison sélectionnée
   */
  public onChangeSeason(season:Season):void {
    this.selectedSeason = season;
    Logger.trace(this, "saison selectionnée : "+this.selectedSeason.name);
  }

  public onClickTournament(tournamentScore:TournamentScoreWrapper) {
    this.router.navigate(['/tournois/'+tournamentScore.id_tournament]);
  }

}
