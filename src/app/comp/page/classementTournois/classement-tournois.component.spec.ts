import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassementTournoisComponent } from './classement-tournois.component';

describe('ClassementTournoisComponent', () => {
  let component: ClassementTournoisComponent;
  let fixture: ComponentFixture<ClassementTournoisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClassementTournoisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassementTournoisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
