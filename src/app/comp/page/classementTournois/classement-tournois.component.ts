import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Tournament } from 'src/app/model/tournament';
import { RankingService } from 'src/app/service/ranking.service';
import { SessionService } from 'src/app/service/session.service';
import { Logger } from 'src/app/tools/logger';

@Component({
  selector: 'app-classement-tournois',
  templateUrl: './classement-tournois.component.html',
  styleUrls: ['./classement-tournois.component.css']
})
export class ClassementTournoisComponent implements OnInit {


  constructor(
    private route:ActivatedRoute,
    private router:Router,
    private sessionService:SessionService,
    private rankingService:RankingService,
  ) { }

  public tournament:Tournament = new Tournament();

  ngOnInit(): void {
    let id_tournament = this.route.snapshot.params['id'];
    if(this.sessionService.selectedTournament && this.sessionService.selectedTournament.id == id_tournament) {
      // si le tournois en session est le bon, alors on l'affecte directement.
      this.tournament = this.sessionService.selectedTournament;
    } else {
      // sinon, on le recharge de la base
      this.iniTournament(id_tournament);
    }

  }

  private iniTournament(id_tournament:number):void {
      this.rankingService.getTournament(id_tournament).subscribe(result => {
        this.tournament = result;
        Logger.trace(this, 'tournois chargé', this.tournament);
      }, (error) => {
        Logger.fatal(this, 'Erreur lors du chargement du tournois', error);
        alert('Erreur lors du tournois');
      });
  }

}
