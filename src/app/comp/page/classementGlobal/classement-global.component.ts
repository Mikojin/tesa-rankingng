import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Game } from 'src/app/model/game';
import { Season } from 'src/app/model/season';
import { SessionService } from 'src/app/service/session.service';
import { Logger } from 'src/app/tools/logger';
import { Tools } from 'src/app/tools/tools';

@Component({
  selector: 'app-classement-global',
  templateUrl: './classement-global.component.html',
  styleUrls: ['./classement-global.component.css']
})
export class ClassementGlobalComponent implements OnInit {

  constructor(
    private route:ActivatedRoute,
    private router:Router,
    private sessionService:SessionService,
  ) { }

  public selectedGame:Game = null;
  public selectedSeason:Season = null;

  ngOnInit(): void {
    this.onOpen();
  }


  private async onOpen() {
    // on s'assure que la session est initialisée
    while(!this.sessionService.initialised) {
      await Tools.wait(500);
    }
    this.selectedGame = this.sessionService.selectedGame;
    this.selectedSeason = this.sessionService.selectedSeason;
  }

  //==========================================================
  //     On Change methods
  //==========================================================

  /**
   *
   * @param game le nouveau jeu sélectionné
   */
  public onChangeGame(game:Game):void {
    this.selectedGame = game;
    Logger.trace(this, "jeu selectionné : "+this.selectedGame.name);
  }

  /**
   *
   * @param season la nouvelle saison sélectionnée
   */
  public onChangeSeason(season:Season):void {
    this.selectedSeason = season;
    Logger.trace(this, "saison selectionnée : "+this.selectedSeason.name);
  }


}
