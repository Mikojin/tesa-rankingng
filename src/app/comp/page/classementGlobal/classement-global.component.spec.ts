import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassementGlobalComponent } from './classement-global.component';

describe('ClassementGlobalComponent', () => {
  let component: ClassementGlobalComponent;
  let fixture: ComponentFixture<ClassementGlobalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClassementGlobalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassementGlobalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
