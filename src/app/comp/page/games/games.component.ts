import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Character } from 'src/app/model/character';
import { Game } from 'src/app/model/game';
import { SessionService } from 'src/app/service/session.service';
import { AbstractComponent } from 'src/app/tools/abstract-component';
import { Logger } from 'src/app/tools/logger';
import { Tools } from 'src/app/tools/tools';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})
export class GamesComponent extends AbstractComponent implements OnInit {

  constructor(
    private route:ActivatedRoute,
    private router:Router,
    private sessionService:SessionService,
  ) {
    super();
  }

  public characterList:Array<Character> = new Array();

  private _selectedGame:Game = null;
  set selectedGame(g:Game) {
    this._selectedGame = g;
    this.updateCharacterList();
  }
  get selectedGame() {
    return this._selectedGame;
  }

  ngOnInit(): void {
    this.onOpen();
  }

  private async onOpen() {
    // on s'assure que la session est initialisée
    while(!this.sessionService.initialised) {
      await Tools.wait(500);
    }
    this.selectedGame = this.sessionService.selectedGame;
  }


  //==========================================================
  //     On Change methods
  //==========================================================

  /**
   *
   * @param game le nouveau jeu sélectionné
   */
  public onChangeGame(game:Game):void {
    Logger.trace(this, "jeu selectionné : "+this.selectedGame.name);
    this.selectedGame = game;
  }


  private updateCharacterList() {
    this.characterList = Array.from(this.selectedGame.characterMap.values());
    this.characterList.sort((a, b) => ((a.name > b.name) ? 1 : ((a.name < b.name) ? -1 : 0))
    );
  }
}
