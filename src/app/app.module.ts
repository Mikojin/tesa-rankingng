import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { HeaderComponent } from './comp/header/header.component';
import { AccueilComponent } from './comp/page/accueil/accueil.component';
import { NewsComponent } from './comp/page/news/news.component';
import { AgendaComponent } from './comp/page/agenda/agenda.component';
import { ClassementGlobalComponent } from './comp/page/classementGlobal/classement-global.component';
import { TournoisComponent } from './comp/page/tournois/tournois.component';
import { VideosComponent } from './comp/page/videos/videos.component';
import { JoueursComponent } from './comp/page/joueurs/joueurs.component';
import { SectionAgendaComponent } from './comp/section/section-agenda/section-agenda.component';
import { HighlightComponent } from './comp/section/highlight/highlight.component';
import { SectionClassementComponent } from './comp/section/section-classement/section-classement.component';
import { SectionNewsComponent } from './comp/section/section-news/section-news.component';
import { SectionVideosComponent } from './comp/section/section-videos/section-videos.component';
import { SessionService } from './service/session.service';
import { FormsModule } from '@angular/forms';
import { GameSelectorComponent } from './comp/share/game-selector/game-selector.component';
import { SeasonSelectorComponent } from './comp/share/season-selector/season-selector.component';
import { ClassementComponent } from './comp/share/classement/classement.component';
import { ClassementTournoisComponent } from './comp/page/classementTournois/classement-tournois.component';
import { JoueurComponent } from './comp/page/joueur/joueur.component';
import { ContactComponent } from './comp/page/contact/contact.component';
import { AproposComponent } from './comp/page/apropos/apropos.component';
import { ImgPipe } from './pipe/img.pipe';
import { GamesComponent } from './comp/page/games/games.component';
import { ImgCharPipe } from './pipe/img-char.pipe';
import { ButtonTesaComponent } from './comp/share/button-tesa/button-tesa.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AccueilComponent,
    NewsComponent,
    AgendaComponent,
    ClassementGlobalComponent,
    TournoisComponent,
    VideosComponent,
    JoueursComponent,
    SectionAgendaComponent,
    HighlightComponent,
    SectionClassementComponent,
    SectionNewsComponent,
    SectionVideosComponent,
    GameSelectorComponent,
    SeasonSelectorComponent,
    ClassementComponent,
    ClassementTournoisComponent,
    JoueurComponent,
    ContactComponent,
    AproposComponent,
    ImgPipe,
    GamesComponent,
    ImgCharPipe,
    ButtonTesaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [SessionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
