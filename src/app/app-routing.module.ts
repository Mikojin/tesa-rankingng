import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccueilComponent } from './comp/page/accueil/accueil.component';
import { AgendaComponent } from './comp/page/agenda/agenda.component';
import { AproposComponent } from './comp/page/apropos/apropos.component';
import { ClassementGlobalComponent } from './comp/page/classementGlobal/classement-global.component';
import { ClassementTournoisComponent } from './comp/page/classementTournois/classement-tournois.component';
import { ContactComponent } from './comp/page/contact/contact.component';
import { GamesComponent } from './comp/page/games/games.component';
import { JoueurComponent } from './comp/page/joueur/joueur.component';
import { JoueursComponent } from './comp/page/joueurs/joueurs.component';
import { NewsComponent } from './comp/page/news/news.component';
import { TournoisComponent } from './comp/page/tournois/tournois.component';
import { VideosComponent } from './comp/page/videos/videos.component';

const routes: Routes = [
  {path: 'accueil',       component: AccueilComponent},
  {path: 'news',          component: NewsComponent},
  {path: 'agenda',        component: AgendaComponent},
  {path: 'classement',    component: ClassementGlobalComponent},
  {path: 'tournois',      component: TournoisComponent},
  {path: 'videos',        component: VideosComponent},
  {path: 'joueurs',       component: JoueursComponent},
  {path: 'jeux',          component: GamesComponent},
  {path: 'joueurs/:id',   component: JoueurComponent},
  {path: 'tournois/:id',  component: ClassementTournoisComponent},
  {path: 'contact',       component: ContactComponent},
  {path: 'apropos',       component: AproposComponent},
  // path par defaut
  {path: '', redirectTo:'/accueil', pathMatch:'full' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
