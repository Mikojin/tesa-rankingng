-- MariaDB dump 10.17  Distrib 10.5.1-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: 172.17.0.11    Database: tesatnsimbtesadb
-- ------------------------------------------------------
-- Server version	5.6.50-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;



--
-- Temporary table structure for view `tournament_score`
--

DROP TABLE IF EXISTS `tournament_score`;
/*!50001 DROP VIEW IF EXISTS `tournament_score`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `tournament_score` (
  `id_game` tinyint NOT NULL,
  `id_tournament` tinyint NOT NULL,
  `tournament_group_name` tinyint NOT NULL,
  `tournament_name` tinyint NOT NULL,
  `date_start` tinyint NOT NULL,
  `date_end` tinyint NOT NULL,
  `id_type_score` tinyint NOT NULL,
  `type_score_name` tinyint NOT NULL,
  `id_player` tinyint NOT NULL,
  `ranking` tinyint NOT NULL,
  `score` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;


--
-- Final view structure for view `tournament_score`
--

/*!50001 DROP TABLE IF EXISTS `tournament_score`*/;
/*!50001 DROP VIEW IF EXISTS `tournament_score`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
-- /*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `tournament_score` AS select `t`.`id_game` AS `id_game`,`t`.`id` AS `id_tournament`,`t`.`group_name` AS `tournament_group_name`,`t`.`name` AS `tournament_name`,`t`.`date_start` AS `date_start`,`t`.`date_end` AS `date_end`,`ts`.`id` AS `id_type_score`,`ts`.`type_name` AS `type_score_name`,`pp`.`id_player` AS `id_player`,`pp`.`ranking` AS `ranking`,`s`.`score` AS `score` from (((`tournament` `t` join `participant` `pp` on((`pp`.`id_tournament` = `t`.`id`))) join `type_score` `ts` on((`ts`.`id` = `t`.`id_type_score`))) left join `scoring` `s` on(((`s`.`id_type_score` = `ts`.`id`) and (`pp`.`ranking` >= `s`.`rank_top`) and (`pp`.`ranking` <= `s`.`rank_bottom`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
