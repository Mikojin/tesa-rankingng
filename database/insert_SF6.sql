-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: tesatnsimbtesa2.mysql.db
-- Generation Time: Oct 07, 2023 at 09:20 PM
-- Server version: 5.7.42-log
-- PHP Version: 8.1.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- Insert SF6 game into the database

INSERT INTO `game` (`id`, `cd_game`, `name`, `tstp_create`, `tstp_update`) VALUES
(11, 'SF6', 'Street Fighter 6', '2023-10-07 14:46:00', '2023-10-07 14:46:00');

-- Insert SF6 characters

INSERT INTO `character` (`id`, `id_game`, `name`, `cd_char`, `tstp_create`, `tstp_update`) VALUES
(224, 11, 'Luke', 'luke', '2023-10-07 14:55:32', '2023-10-07 14:55:32'),
(225, 11, 'Blanka', 'blanka', '2023-10-07 14:55:32', '2023-10-07 14:55:32'),
(226, 11, 'A.K.I', 'aki', '2023-10-07 14:55:45', '2023-10-07 14:55:45'),
(227, 11, 'Cammy', 'cammy', '2023-10-07 15:04:07', '2023-10-07 15:04:07'),
(228, 11, 'Chun Li', 'chunli', '2023-10-07 15:04:07', '2023-10-07 15:08:58'),
(229, 11, 'Dee Jay', 'deejay', '2023-10-07 15:05:31', '2023-10-07 15:09:05'),
(230, 11, 'Dhalsim', 'dhalsim2', '2023-10-07 15:05:31', '2023-10-07 18:36:16'),
(233, 11, 'E. Honda', 'ehonda', '2023-10-07 15:06:13', '2023-10-07 15:06:13'),
(234, 11, 'Guile', 'guile', '2023-10-07 15:06:13', '2023-10-07 15:06:13'),
(235, 11, 'JP', 'jp', '2023-10-07 15:06:43', '2023-10-07 15:06:43'),
(236, 11, 'Jamie', 'jamie', '2023-10-07 15:06:43', '2023-10-07 15:06:43'),
(237, 11, 'Juri', 'juri', '2023-10-07 15:07:12', '2023-10-07 15:07:12'),
(238, 11, 'Ken', 'ken', '2023-10-07 15:07:12', '2023-10-07 15:07:12'),
(239, 11, 'Kimberly', 'kimberly', '2023-10-07 15:12:14', '2023-10-07 15:12:14'),
(240, 11, 'Lily', 'lily', '2023-10-07 15:12:14', '2023-10-07 15:12:14'),
(241, 11, 'Manon', 'manon', '2023-10-07 15:12:41', '2023-10-07 18:55:05'),
(242, 11, 'Marisa', 'marisa', '2023-10-07 15:12:41', '2023-10-07 15:12:41'),
(243, 11, 'Rashid', 'rashid', '2023-10-07 15:13:07', '2023-10-07 15:13:07'),
(244, 11, 'Ryu', 'ryu', '2023-10-07 15:13:07', '2023-10-07 15:13:07'),
(245, 11, 'Zangief', 'zangief', '2023-10-07 15:13:23', '2023-10-07 15:13:23');
