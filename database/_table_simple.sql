CREATE TABLE `user` (
  `login` varchar(40) CHARACTER SET latin1 NOT NULL,
  `password` varchar(40) CHARACTER SET latin1 NOT NULL,
  `right` varchar(30) CHARACTER SET latin1 NOT NULL,
  `tstp_create` timestamp NOT NULL,
  `tstp_update` timestamp NOT NULL,
  UNIQUE KEY `login` (`login`),
  KEY `login_2` (`login`)
);


CREATE TABLE `game` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) CHARACTER SET latin1 NOT NULL,
  `name` varchar(150) CHARACTER SET latin1 NOT NULL,
  `id_char_unknown` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `GAME_CHAR_UNKNOWN` (`id_char_unknown`),
  CONSTRAINT `GAME_CHAR_UNKNOWN` FOREIGN KEY (`id_char_unknown`) REFERENCES `character` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE `character` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_game` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `css_class` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `filename` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `tstp_create` timestamp NOT NULL,
  `tstp_update` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `character_fk1` (`id_game`),
  CONSTRAINT `character_fk1` FOREIGN KEY (`id_game`) REFERENCES `game` (`id`)
);

CREATE TABLE `player` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` char(1) CHARACTER SET latin1 DEFAULT NULL,
  `pseudo` varchar(255) CHARACTER SET latin1 NOT NULL,
  `nom` varchar(255) CHARACTER SET latin1 NOT NULL,
  `prenom` varchar(255) CHARACTER SET latin1 NOT NULL,
  `mail` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `telephone` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `tstp_create` timestamp NOT NULL,
  `tstp_update` timestamp NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `player_game` (
  `id_player` int(11) NOT NULL,
  `id_game` int(11) NOT NULL,
  `id_character` int(11) NOT NULL DEFAULT '32',
  KEY `player_game_fk01` (`id_game`),
  KEY `player_game_fk02` (`id_player`),
  KEY `player_game_fk03` (`id_character`),
  CONSTRAINT `player_game_fk01` FOREIGN KEY (`id_game`) REFERENCES `game` (`id`) ON DELETE CASCADE,
  CONSTRAINT `player_game_fk02` FOREIGN KEY (`id_player`) REFERENCES `player` (`id`) ON DELETE CASCADE,
  CONSTRAINT `player_game_fk03` FOREIGN KEY (`id_character`) REFERENCES `character` (`id`) ON DELETE CASCADE
);

CREATE TABLE `season` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  PRIMARY KEY (`id`)
);


CREATE TABLE `type_score` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(50) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
);


CREATE TABLE `scoring` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_type_score` int(11) NOT NULL,
  `rank_top` int(11) NOT NULL,
  `rank_bottom` int(11) NOT NULL DEFAULT '999999',
  `score` int(11) NOT NULL,
  `tstp_create` timestamp NOT NULL,
  `tstp_update` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_type_score` (`id_type_score`,`rank_top`),
  UNIQUE KEY `scoring_idx01` (`id_type_score`,`rank_top`,`rank_bottom`),
  CONSTRAINT `scoring_fk1` FOREIGN KEY (`id_type_score`) REFERENCES `type_score` (`id`)
);



CREATE TABLE `tournament` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_game` int(11) NOT NULL,
  `group_name` varchar(250) CHARACTER SET latin1 NOT NULL,
  `name` varchar(250) CHARACTER SET latin1 NOT NULL,
  `id_type_score` int(11) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date DEFAULT NULL,
  `tstp_create` timestamp NOT NULL,
  `tstp_update` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_name` (`group_name`),
  KEY `group_name_2` (`group_name`,`date_start`),
  KEY `date_start` (`date_start`),
  KEY `tournament_fk1` (`id_game`),
  KEY `tournament_fk2` (`id_type_score`),
  CONSTRAINT `tournament_fk1` FOREIGN KEY (`id_game`) REFERENCES `game` (`id`),
  CONSTRAINT `tournament_fk2` FOREIGN KEY (`id_type_score`) REFERENCES `type_score` (`id`)
);

CREATE TABLE `participant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tournament` int(11) NOT NULL,
  `id_player` int(11) NOT NULL,
  `ranking` int(11) NOT NULL DEFAULT '0',
  `tstp_create` timestamp NOT NULL,
  `tstp_update` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `participant_fk1` (`id_tournament`),
  CONSTRAINT `participant_fk1` FOREIGN KEY (`id_tournament`) REFERENCES `tournament` (`id`) ON DELETE CASCADE,
  CONSTRAINT `participant_fk2` FOREIGN KEY (`id_player`) REFERENCES `player` (`id`) ON DELETE CASCADE

);