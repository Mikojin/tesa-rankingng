
--
-- Dumping data for table `game`
--

INSERT INTO `game`
(`id`, `cd_game`, `name`)
VALUES
(1,'SFV','Street Fighter V'),
(2,'T7','Tekken 7'),
(3,'DBFZ','Dragon Ball FighterZ'),
(4,'KOFXIV','The King of Fighters XIV'),
(5,'MVCI','Marvel vs Capcom Infinite'),
(6,'GGXR','Guilty Gear Xrd Revelator'),
(7,'SFIV','Street Fighter IV'),
(8,'MVC3','Marvel vs Capcom 3'),
(9,'SSBU','Super Smash Bros Ultimate'),
(10,'SamSho','Samurai Shodown');


