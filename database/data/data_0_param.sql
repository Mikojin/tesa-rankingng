
--
-- Dumping data for table `param`
--

INSERT INTO `param`
(`group_name`, `variable`,`value`,`tstp_create`,`tstp_update`)
VALUES
('FONT','font_name','hvd_comic_serif_pro','2018-08-30 21:48:04','2018-08-30 21:48:04'),
('FONT','font_pseudo','hvd_comic_serif_pro','2018-08-30 21:48:04','2018-09-21 12:13:19'),
('FONT','font_rank','painting_with_chocolate','2018-08-30 21:48:04','2018-09-21 12:12:28'),
('FONT','font_score','levirebrushed','2018-08-30 21:48:04','2018-08-30 21:48:04'),
('PATH','character','/ranking/Images/Character','2018-09-01 21:05:15','2019-02-09 15:03:37');
