[![pipeline status](https://gitlab.com/Mikojin/tesa-rankingng/badges/dev/pipeline.svg)](https://gitlab.com/Mikojin/tesa-rankingng/-/commits/dev)
# ##############################
# TesaRankingNg


To build for production :
`ng build --configuration=prod --base-href /ranking-ng/ --deploy-url /ranking-ng/`
Where the final URL looks like this `http://www.tesa.tn/ranking-ng/`

To build for beta :
`ng build --configuration=beta --base-href /beta/ --deploy-url /beta/`

To build for dev :
`ng build --configuration=dev --base-href /tesa-ranking-ng-prod/ --deploy-url /tesa-ranking-ng-prod/`


Check angular.json "outputPath" to configure the path where the distribution is generated

Check environment.prod.ts to define the apiUrl in production environment.

url in css file should redirect to assets relatively.
example : 
  /assets/img/test.png
  /app/comp/page/classement/classement.component.css

  in the css, to point to test.png : `url(./../../../../assets/images/test.png`

url of images in html file should use the img pipe that automatiquely start the path at `assets/images/`
so you should use `{{ 'backgrounds/ryu.jpg' | img }}` instead of `"assets/images/backgrounds/ryu.jpg"`


## ##############################
## Auto Generated information

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
